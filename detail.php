<?php
require_once 'config.php';
require_once ROOT_PATH.'/lib/dao_utility.php';
require_once ROOT_PATH.'/lib/mysqlDao.php';
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--[if IE]><link rel="icon" href="https://suzukicdn.net/themes/default2019//favicon.ico"><![endif]-->
    <title>Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia</title>
    <meta name="description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="google-site-verification" content="qStaE60pis5fkmA_6n4pOgeqXgvmFiws7fYMCkxL5Fc" />

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta property="og:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta property="og:url" content="https://www.suzuki.co.id/marine">
    <meta property="og:image" content="https://suzukicdn.net/themes/default2019/img/logo.png?1.1.25">
    <meta property="og:site_name" content="Suzuki Indonesia">
    <meta property="og:see_also" content="https://twitter.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.facebook.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.instagram.com/suzuki_id">

    <meta name="twitter:site" content="https://www.suzuki.co.id/marine">
    <meta name="twitter:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta name="twitter:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="twitter:image" content="https://suzukicdn.net/themes/default2019/img/logo.png?1.1.25">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:creator" content="@Suzukiindonesia">
    <link rel="canonical" href="https://www.suzuki.co.id/marine">

    <link href="https://suzukicdn.net/themes/default2019/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/default2019/css/aos.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/default2019/css/reset.min.css?v=1.1.25" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/default2019/css/style.min.css?v=1.1.25" rel="stylesheet">
    <link href="<?php echo ROOT_URL?>/assets/themes/default2019/css/newMarineDefault2019.css?<?php echo rand()?>"
        rel="stylesheet">

    <link rel="icon" type="image/png" sizes="32x32"
        href="https://suzukicdn.net/themes/default2019/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16"
        href="https://suzukicdn.net/themes/default2019/icons/favicon-16x16.png">
    <!-- <link rel="manifest" href="https://suzukicdn.net/themes/default2019//manifest.json"> -->
    <meta name="theme-color" content="#173e81">
    <meta name="apple-mobile-web-app-capable" content="no">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="apple-mobile-web-app-title" content="Suzuki Indonesia">
    <link rel="apple-touch-icon" href="https://suzukicdn.net/themes/default2019/icons/apple-touch-icon-152x152.png">
    <meta name="msapplication-TileImage"
        content="https://suzukicdn.net/themes/default2019/icons/msapplication-icon-144x144.png">
    <meta name="msapplication-TileColor" content="#173e81">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js"></script>
    <script type="text/javascript">
    var base_url = 'https://www.suzuki.co.id/';
    </script>
    <script src="https://suzukicdn.net/themes/default2019/js/jquery.min.js"></script>
</head>

<body>
    <div id="app">
        <?php require_once 'include/header.php'; ?>
        <main id="product" class="mt-0 product-marine newMarine2019 detailnewMarine2019">


            <section class="hero hero-product hero-detail">
                <img src="<?php echo ROOT_URL?>/assets/img/background/hero-detail.jpg?<?php echo rand()?>" alt=""
                    class="w-100">
                <div class="caption caption-detail">
                    <div class="container">
                        <div class="row ai-center">
                            <div class="col-4">
                                <h2 class="mb-10">Suzuki DF325ATX</h2>
                                <p class="mb-0">SUZUKI SUZUKI DF325ATX, MESIN KAPAL DENGAN BIG ENGINE (4 STROKE
                                    OUTBOARDS)
                                </p>
                            </div>
                            <div class="col-5">
                                <img src="<?php echo ROOT_URL?>/assets/img/detail/detail.png?<?php echo rand()?>"
                                    class="w-100" alt="">
                            </div>
                            <div class="col-3">
                                <img src="https://suzukicdn.net/themes/default2019/img/marine/logo-ultimate.png" alt=""
                                    class="" style="width:100px">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="product-list section-white">
                <div class="container">
                    <div class="align-center mt-100 mb-50">
                        <h2>Suzuki Marine</h2>
                        <p>Lorem ipsum is common placeholder text used to demonstrate the graphic<br> elements of a
                            document or visual presentation.</p>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <ul class="feature">
                                <li>
                                    <div class="feature-inner">
                                        <h6>DURABILITY & RELIABILITY</h6>
                                        <p>Lorem ipsum dolor sit amet, consectetur.</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="feature-inner">
                                        <h6>PERFORMANCE</h6>
                                        <p>Lorem ipsum dolor sit amet, consectetur.</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="feature-inner">
                                        <h6>EASE & COMFORT</h6>
                                        <p>Lorem ipsum dolor sit amet, consectetur.</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="feature-inner">
                                        <h6>ECOLOGY & ECONOMY</h6>
                                        <p>Lorem ipsum dolor sit amet, consectetur.</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </section>

            <section class="product-list section-tech">
                <div class="container">
                    <div class="row">
                        <div class="col-4" style="align-self:center">
                            <?php 
                                for ($i=0; $i < 3; $i++) { 
                                    # code...
                                
                           ?>
                            <div class="tech-box">
                                <h5>Latest Technology</h5>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit volu atem accusam.</p>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="col-4" style="align-self:center">
                            <img src="<?php echo ROOT_URL?>/assets/img/detail/engine.png?<?php echo rand()?>"
                                style="width:200px;" alt="">
                        </div>
                        <div class="col-4" style="align-self:center">
                            <?php 
                                for ($i=0; $i < 3; $i++) { 
                                    # code...
                                
                           ?>
                            <div class="tech-box">
                                <h5>Latest Technology</h5>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit volu atem accusam.</p>
                            </div>
                            <?php } ?>
                        </div>
                    </div>

                </div>
            </section>

            <section class="product-list section-feature">
                <div class="container">
                    <div class="align-center mt-50">
                        <h2>Technology</h2>
                        <p>Lorem ipsum is common placeholder text used to demonstrate the graphic.</p>
                        <div class="feature-icons">
                            <a href="javascript:void(0)" data-target="#offset_driveshaft"><img alt=""
                                    src="https://i.ibb.co/QXj3Nbr/Feature-Icon-with-circle.png" alt="Offset Driveshaft"
                                    class="pr-1 pb-1"></a>
                            <a href="javascript:void(0)" data-target="#gear_ratio"><img alt=""
                                    src="https://i.ibb.co/QXj3Nbr/Feature-Icon-with-circle.png"
                                    alt="2-Stage Gear Reduction" class="pr-1 pb-1"></a>
                            <a href="javascript:void(0)" data-target="#vvt"><img alt=""
                                    src="https://i.ibb.co/QXj3Nbr/Feature-Icon-with-circle.png"
                                    alt="VVT (Variable Valve Timing)" class="pr-1 pb-1"></a>
                            <a href="javascript:void(0)" data-target="#direct_intake"><img alt=""
                                    src="https://i.ibb.co/QXj3Nbr/Feature-Icon-with-circle.png" alt="Direct Air Intake"
                                    class="pr-1 pb-1"></a>
                            <a href="javascript:void(0)" data-target="#self_adjusting_timing_chain"><img alt=""
                                    src="https://i.ibb.co/QXj3Nbr/Feature-Icon-with-circle.png"
                                    alt="Self Adjusting Timing Chain" class="pr-1 pb-1"></a>
                            <a href="javascript:void(0)" data-target="#twoway_water_inlet"><img alt=""
                                    src="https://i.ibb.co/QXj3Nbr/Feature-Icon-with-circle.png"
                                    alt="Two-Way Water Inlet" class="pr-1 pb-1"></a>
                            <a href="javascript:void(0)" data-target="#dual_louver"><img alt=""
                                    src="https://i.ibb.co/QXj3Nbr/Feature-Icon-with-circle.png"
                                    alt="Suzuki Dual Louver System" class="pr-1 pb-1"></a>
                            <a href="javascript:void(0)" data-target="#lean_burn"><img alt=""
                                    src="https://i.ibb.co/QXj3Nbr/Feature-Icon-with-circle.png" alt="Lean Burn"
                                    class="pr-1 pb-1"></a>

                        </div>
                        <div class="feature-description mt-50">
                            <div id="offset_driveshaft" style="display:none">
                                <img src="https://i.ibb.co/vdw1KFS/image-681.png" class="" alt="">
                                <p>Lorem ipsum is common placeholder text used to demonstrate the graphic elements of a
                                    document or visual presentation.</p>
                            </div>
                            <div id="gear_ratio" style="display:none">
                                <img src="https://i.ibb.co/vdw1KFS/image-681.png" class="" alt="">
                                <p>Lorem ipsum is common placeholder text used to demonstrate the graphic elements of a
                                    document or visual presentation.</p>
                            </div>
                            <div id="vvt" style="display:none">
                                <img src="https://i.ibb.co/vdw1KFS/image-681.png" class="" alt="">
                                <p>Lorem ipsum is common placeholder text used to demonstrate the graphic elements of a
                                    document or visual presentation.</p>
                            </div>
                            <div id="direct_intake" style="display:none">
                                <img src="https://i.ibb.co/vdw1KFS/image-681.png" class="" alt="">
                                <p>Lorem ipsum is common placeholder text used to demonstrate the graphic elements of a
                                    document or visual presentation.</p>
                            </div>
                            <div id="self_adjusting_timing_chain" style="display:none">
                                <img src="https://i.ibb.co/vdw1KFS/image-681.png" class="" alt="">
                                <p>Lorem ipsum is common placeholder text used to demonstrate the graphic elements of a
                                    document or visual presentation.</p>
                            </div>
                            <div id="twoway_water_inlet" style="display:none">
                                <img src="https://i.ibb.co/vdw1KFS/image-681.png" class="" alt="">
                                <p>Lorem ipsum is common placeholder text used to demonstrate the graphic elements of a
                                    document or visual presentation.</p>
                            </div>
                            <div id="dual_louver" style="display:none">
                                <img src="https://i.ibb.co/vdw1KFS/image-681.png" class="" alt="">
                                <p>Lorem ipsum is common placeholder text used to demonstrate the graphic elements of a
                                    document or visual presentation.</p>
                            </div>
                            <div id="lean_burn" style="display:none">
                                <img src="https://i.ibb.co/vdw1KFS/image-681.png" class="" alt="">
                                <p>Lorem ipsum is common placeholder text used to demonstrate the graphic elements of a
                                    document or visual presentation.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section class="product-prices">
                <div class="container">
                    <div class="align-center mt-50">
                        <h2>Daftar Harga</h2>
                        <p>Harga On The Road Jakarta Mei 2021<br>
                            <small>*Harga tidak mengikat, dapat berubah sewaktu-waktu</small>
                        </p>
                        <div class="mt-30 mb-30">
                            <div class="group-model">DF 325 ATX</div>
                            <div class="price">Rp. 404.800.000</div>
                        </div>
                        <div class="group-button">
                            <a href="" class="btn btn-outline-black">Unduh Brosur</a>
                            <a href="" class="btn btn-outline-black">Spesifikasi</a>
                        </div>
                    </div>

                </div>
            </section>

            <section class="product-list section-gallery">
                <div class="container">
                    <div class="align-center mt-50">
                        <h2>Galeri</h2>
                        <div class="row">
                            <?php 
                                for ($i=0; $i < 6; $i++) { 
                                    # code...
                                
                            ?>
                            <div class="col-4">
                                <div class="gallery-item">
                                    <img src="https://i.ibb.co/fQjW7sL/image-63.png" class="w-100" alt="">
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </section>



        </main>
        <script>
        var type = 'marine';
        </script>
        <footer id="footer">
            <div class="container">
                <div class="footer-nav">
                    <div class="nav-section">
                        <h3>Produk &amp; Layanan</h3>
                        <ul>
                            <li><a href="https://www.suzuki.co.id/automobile">Mobil Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/motorcycle">Motor Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/marine">Marine Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/pricelist">Daftar Harga Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/dealers">Temukan Dealer Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/services">Servis Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/eparts">Suku Cadang Suzuki</a></li>
                            <li><a href="https://dms.suzuki.co.id/simdms/assets/custom/productQualityUpdate.cshtml">Suzuki
                                    Quality Update</a></li>
                        </ul>
                    </div>
                    <div class="nav-section">
                        <h3>Pojok Suzuki</h3>
                        <ul>
                            <li><a href="https://www.suzuki.co.id/club/car">Car Club</a></li>
                            <li><a href="https://www.suzuki.co.id/club/motor">Motor Club</a></li>
                            <li><a href="https://www.suzuki.co.id/promo">Promo</a></li>
                            <li><a href="https://www.suzuki.co.id/news">Press Release</a></li>
                            <li><a href="https://www.suzuki.co.id/tips-trik">Tips and Trick</a></li>
                            <li><a href="https://www.suzuki.co.id/mysuzukistory">My Suzuki Story</a></li>
                            <li><a href="http://bikerscommunitysuzuki.com">Bikers Community</a></li>
                            <li><a href="https://suzukigsxseries.com">GSX Series</a></li>
                        </ul>
                    </div>
                    <div class="nav-section">
                        <h3>Lainnya</h3>
                        <ul>
                            <li><a href="https://www.suzuki.co.id/contact">Hubungi Kami</a></li>
                            <li><a href="http://www.sfi.co.id/">Suzuki Finance Indonesia</a></li>
                            <li><a href="http://www.autovalue.co.id/">Suzuki Autovalue</a></li>
                            <li><a href="https://www.suzuki.co.id/suzuki-insurance">Suzuki Insurance</a></li>
                        </ul>
                    </div>
                    <div class="nav-section">
                        <h3>Suzuki Corporate</h3>
                        <ul>
                            <li><a href="https://www.suzuki.co.id/corporate/tentang-suzuki">Tentang Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/corporate/sejarah">Sejarah</a></li>
                            <li><a href="https://www.suzuki.co.id/kunjungan-pabrik">Kunjungan Pabrik</a></li>
                            <li><a href="https://www.suzuki.co.id/corporate/penghargaan">Penghargaan</a></li>
                            <li><a href="https://www.suzuki.co.id/corporate/karir">Karir</a></li>
                            <li><a href="http://www.globalsuzuki.com/">Suzuki Global</a></li>
                            <li><a href="https://www.suzuki.co.id/corporate/csr">CSR</a></li>
                        </ul>
                    </div>
                    <div class="nav-section">
                        <h3>Hubungi Kami</h3>
                        <p class="fs-20 mb-5">Halo Suzuki<br><strong><a
                                    href="tel:0800-1100-800">0800-1100-800</a></strong></p>
                        <p class="fs-12 text-muted">Bebas Pulsa, 24 Jam Siaga Melayani Anda</p>
                        <a href="https://play.google.com/store/apps/details?id=com.sim.mysuzuki" target="_blank">
                            <span>Download My Suzuki</span>
                            <img alt="" src="https://suzukicdn.net/themes/default2019/img/mysuzuki.png" alt="">

                        </a>
                        <a href="https://www.suzuki.co.id/ecstar" target="_blank">
                            <img alt="" src="https://suzukicdn.net/themes/default2019/img/ecstar3.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="footer-nav row">
                    <div class="col-6">
                        <h3>suzuki mobil</h3>
                        <p class="fs-24">
                            <a href="https://id-id.facebook.com/suzukiindonesia" target="_blank" class="mr-5">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a href="https://twitter.com/suzukiindonesia" target="_blank" class="mr-5">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="https://www.instagram.com/suzuki_id" target="_blank" class="mr-5">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a href="https://www.youtube.com/user/SuzukiID" target="_blank" class="mr-5">
                                <i class="fa fa-youtube"></i>
                            </a>
                        </p>
                    </div>
                    <div class="col-6">
                        <h3>suzuki motor</h3>
                        <p class="fs-24">
                            <a href="https://id-id.facebook.com/SuzukiMotorcyclesID" target="_blank" class="mr-5">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a href="https://twitter.com/suzukimotorid" target="_blank" class="mr-5">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="https://www.instagram.com/suzukiindonesiamotor" target="_blank" class="mr-5">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a href="https://www.youtube.com/suzukimotorcyclesindonesia" target="_blank" class="mr-5">
                                <i class="fa fa-youtube"></i>
                            </a>
                        </p>
                    </div>
                </div>
                <div class="copyright scbottom">© 2019 Suzuki Indonesia. All rights reserved.</div>
            </div>
        </footer>
    </div>
    <script src="https://suzukicdn.net/themes/default2019/js/aos.js"></script>
    <script src="https://suzukicdn.net/themes/default2019/js/owl.carousel.min.js"></script>
    <script src="https://suzukicdn.net/themes/default2019/js/app.min.js?v=1.1.25"></script>
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>

    <script src="https://suzukicdn.net/themes/default2019/js/bootstrap.min.js"></script>
    <script src="https://suzukicdn.net/themes/default2019/js/marine.min.js"></script>
    <script>
    $('.feature-description div:first-child').show();
    $('.feature-icons a:first-child').addClass('active');
    $('.feature-icons a').click(function() {
        var target = $(this).attr('data-target');
        $(target).siblings().hide();
        $(target).fadeIn();
        $(this).siblings('.active').removeClass('active');
        $(this).addClass('active');
    })
    </script>

    <!-- <script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=1159525&mt_adid=185618&s1=https://www.suzuki.co.id/&s2=&s3=indonesia'></script> -->
</body>

</html>