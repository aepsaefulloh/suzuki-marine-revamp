<?php
require_once 'config.php';
require_once ROOT_PATH.'/lib/dao_utility.php';
require_once ROOT_PATH.'/lib/mysqlDao.php';
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--[if IE]><link rel="icon" href="https://suzukicdn.net/themes/default2019//favicon.ico"><![endif]-->
    <title>Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia</title>
    <meta name="description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="google-site-verification" content="qStaE60pis5fkmA_6n4pOgeqXgvmFiws7fYMCkxL5Fc" />

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta property="og:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta property="og:url" content="https://www.suzuki.co.id/marine">
    <meta property="og:image" content="https://suzukicdn.net/themes/default2019/img/logo.png?1.1.25">
    <meta property="og:site_name" content="Suzuki Indonesia">
    <meta property="og:see_also" content="https://twitter.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.facebook.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.instagram.com/suzuki_id">

    <meta name="twitter:site" content="https://www.suzuki.co.id/marine">
    <meta name="twitter:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta name="twitter:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="twitter:image" content="https://suzukicdn.net/themes/default2019/img/logo.png?1.1.25">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:creator" content="@Suzukiindonesia">
    <link rel="canonical" href="https://www.suzuki.co.id/marine">

    <link href="https://suzukicdn.net/themes/default2019/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/default2019/css/aos.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/default2019/css/reset.min.css?v=1.1.25" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/default2019/css/style.min.css?v=1.1.25" rel="stylesheet">
    <link href="<?php echo ROOT_URL?>/assets/themes/default2019/css/newMarineDefault2019.css?<?php echo rand()?>"
        rel="stylesheet">

    <link rel="icon" type="image/png" sizes="32x32"
        href="https://suzukicdn.net/themes/default2019/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16"
        href="https://suzukicdn.net/themes/default2019/icons/favicon-16x16.png">
    <!-- <link rel="manifest" href="https://suzukicdn.net/themes/default2019//manifest.json"> -->
    <meta name="theme-color" content="#173e81">
    <meta name="apple-mobile-web-app-capable" content="no">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="apple-mobile-web-app-title" content="Suzuki Indonesia">
    <link rel="apple-touch-icon" href="https://suzukicdn.net/themes/default2019/icons/apple-touch-icon-152x152.png">
    <meta name="msapplication-TileImage"
        content="https://suzukicdn.net/themes/default2019/icons/msapplication-icon-144x144.png">
    <meta name="msapplication-TileColor" content="#173e81">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js"></script>
    <script type="text/javascript">
    var base_url = 'https://www.suzuki.co.id/';
    </script>
    <script src="https://suzukicdn.net/themes/default2019/js/jquery.min.js"></script>
</head>

<body>
    <div id="app">
        <?php require_once 'include/header.php'; ?>
        <main id="product" class="mt-0 product-marine newMarine2019 detailnewMarine2019">


            <section class="hero hero-product hero-detail">
                <img src="<?php echo ROOT_URL?>/assets/img/background/hero-detail.jpg?<?php echo rand()?>" alt=""
                    class="w-100">
                <div class="caption caption-detail">
                    <div class="container">
                        <div class="row ai-center">
                            <div class="col-4">
                                <h2 class="mb-10">Suzuki DF115A </h2>
                                <p class="mb-0">If an engine with performance is an essential part of your boating life,
                                    then you need our combination of quality, reliability and choice.</p>
                            </div>
                            <div class="col-5">
                                <img src="<?php echo ROOT_URL?>/assets/img/detail/detail.png?<?php echo rand()?>"
                                    class="w-100" alt="">
                            </div>
                            <div class="col-3">
                                <img src="https://suzukicdn.net/themes/default2019/img/marine/logo-ultimate.png" alt=""
                                    class="" style="width:100px">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="product-list section-white pt-100">
            </section>

            <div class="tab-box tab-detail-md">
                <div class="tabs" style="background:#1071B8;">
                    <button class="tabs__button tabs__button--active" type="button">Feature</button>
                    <button class="tabs__button" type="button">Technology</button>
                </div>

                <div class="tab-content tab-content--active ">
                    <section class="product-list section-gallery section-engine section-detail-md-tab">
                        <div class="container">
                            <div class="topHeading">
                                <div class="row">
                                    <div class="col-7" style="text-align:start;">
                                        <h2 class="text-white">Maximizing Fuel Economy and Performance</h2>
                                        <p class="text-white">With the launch of the new generation DF140A, DF115A and
                                            DF100A,
                                            our engineers have delivered a trio of outboards that offer great fuel
                                            economy
                                            without sacrificing on performance. This has been achieved by employing our
                                            advanced
                                            Lean Burn Fuel Control System.
                                        </p>
                                    </div>
                                    <div class="col-5">
                                        <div class="num">
                                            <h1>1</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="line1 mt-50 mb-50"></div>
                            <div class="topHeading">
                                <div class="row">
                                    <div class="col-5">

                                        <div class="num">
                                            <h1>2</h1>
                                        </div>
                                    </div>
                                    <div class="col-7" style="text-align:end;">
                                        <h2 class="text-white">Transferring Power Into Speed</h2>
                                        <p class="text-white">Our in-line four cylinder outboards utilise aggressive
                                            gear ratios that enable these outboards to swing a larger prop. The
                                            combination of a large prop and lower gear ratio delivers plenty of torque,
                                            exhilarating acceleration and exciting top-end speed.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="line1 mt-50 mb-50"></div>
                            <div class="topHeading">
                                <div class="row">
                                    <div class="col-7" style="text-align:start;">
                                        <h2 class="text-white">All Torque</h2>
                                        <p class="text-white">With acceleration and manoeuvrability high on the list of
                                            priorities it is no surprise that the DF140, with its excellent power to
                                            weight ratio, was one of the models selected for use at the London 2012
                                            Olympic & Paralympic Games Sailing Regatta.
                                        </p>
                                    </div>
                                    <div class="col-5">
                                        <div class="num">
                                            <h1>3</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="line1 mt-50 mb-50"></div>
                            <div class="topHeading">
                                <div class="row">
                                    <div class="col-5">

                                        <div class="num">
                                            <h1>4</h1>
                                        </div>
                                    </div>
                                    <div class="col-7" style="text-align:end;">
                                        <h2 class="text-white">Innovative Technology</h2>
                                        <p class="text-white">The DF115A include an O2 Sensor Feedback System that helps
                                            to keep emissions cleaner and more stable. In addition to this the DF140A
                                            and DF115A also benefit form a knock sensor that detects and controls
                                            abnormal combustion allowing the engine to operate at optimum performance.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="line1 mt-50 mb-50"></div>
                            <div class="topHeading">
                                <div class="row">
                                    <div class="col-7" style="text-align:start;">
                                        <h2 class="text-white">Cleaner, More Efficient Operation</h2>
                                        <p class="text-white">Suzuki utilizes a number of its advanced technologies to
                                            deliver cleaner, more efficient operation that conforms to the Recreational
                                            Craft Directive (RCD) – Directive 2003/44EC of the European Parliament and
                                            of the Council, and has received three-star ratings from the California Air
                                            Resources Board (CARB).
                                        </p>
                                        <img src="<?php echo ROOT_URL?>/assets/themes/default2019/img/detail/medium/star.png"
                                            alt="">
                                    </div>
                                    <div class="col-5">
                                        <div class="num">
                                            <h1>5</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="tab-content">
                    <section class="product-list section-gallery section-engine section-detail-md-tab">
                        <div class="container">
                            <div class="topHeading">
                                <div class="row">
                                    <div class="col-7" style="text-align:start;">
                                        <h2 class="text-white">Maximizing Fuel Economy and Performance</h2>
                                        <p class="text-white">With the launch of the new generation DF140A, DF115A and
                                            DF100A,
                                            our engineers have delivered a trio of outboards that offer great fuel
                                            economy
                                            without sacrificing on performance. This has been achieved by employing our
                                            advanced
                                            Lean Burn Fuel Control System.
                                        </p>
                                    </div>
                                    <div class="col-5">
                                        <div class="num">
                                            <h1>1</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="line1 mt-50 mb-50"></div>
                            <div class="topHeading">
                                <div class="row">
                                    <div class="col-5">

                                        <div class="num">
                                            <h1>2</h1>
                                        </div>
                                    </div>
                                    <div class="col-7" style="text-align:end;">
                                        <h2 class="text-white">Transferring Power Into Speed</h2>
                                        <p class="text-white">Our in-line four cylinder outboards utilise aggressive
                                            gear ratios that enable these outboards to swing a larger prop. The
                                            combination of a large prop and lower gear ratio delivers plenty of torque,
                                            exhilarating acceleration and exciting top-end speed.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="line1 mt-50 mb-50"></div>
                            <div class="topHeading">
                                <div class="row">
                                    <div class="col-7" style="text-align:start;">
                                        <h2 class="text-white">All Torque</h2>
                                        <p class="text-white">With acceleration and manoeuvrability high on the list of
                                            priorities it is no surprise that the DF140, with its excellent power to
                                            weight ratio, was one of the models selected for use at the London 2012
                                            Olympic & Paralympic Games Sailing Regatta.
                                        </p>
                                    </div>
                                    <div class="col-5">
                                        <div class="num">
                                            <h1>3</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="line1 mt-50 mb-50"></div>
                            <div class="topHeading">
                                <div class="row">
                                    <div class="col-5">

                                        <div class="num">
                                            <h1>4</h1>
                                        </div>
                                    </div>
                                    <div class="col-7" style="text-align:end;">
                                        <h2 class="text-white">Innovative Technology</h2>
                                        <p class="text-white">The DF115A include an O2 Sensor Feedback System that helps
                                            to keep emissions cleaner and more stable. In addition to this the DF140A
                                            and DF115A also benefit form a knock sensor that detects and controls
                                            abnormal combustion allowing the engine to operate at optimum performance.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="line1 mt-50 mb-50"></div>
                            <div class="topHeading">
                                <div class="row">
                                    <div class="col-7" style="text-align:start;">
                                        <h2 class="text-white">Cleaner, More Efficient Operation</h2>
                                        <p class="text-white">Suzuki utilizes a number of its advanced technologies to
                                            deliver cleaner, more efficient operation that conforms to the Recreational
                                            Craft Directive (RCD) – Directive 2003/44EC of the European Parliament and
                                            of the Council, and has received three-star ratings from the California Air
                                            Resources Board (CARB).
                                        </p>
                                        <img src="<?php echo ROOT_URL?>/assets/themes/default2019/img/detail/medium/star.png"
                                            alt="">
                                    </div>
                                    <div class="col-5">
                                        <div class="num">
                                            <h1>5</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

            <section class="product-prices">
                <div class="container">
                    <div class="align-center mt-50">
                        <h2>Daftar Harga</h2>
                        <p>Harga On The Road Jakarta Mei 2021<br>
                            <small>*Harga tidak mengikat, dapat berubah sewaktu-waktu</small>
                        </p>
                        <div class="mt-30 mb-30">
                            <div class="group-model">DF 325 ATX</div>
                            <div class="price">Rp. 404.800.000</div>
                        </div>
                        <div class="group-button">
                            <a href="" class="btn btn-outline-black">Unduh Brosur</a>
                            <a href="" class="btn btn-outline-black">Spesifikasi</a>
                        </div>
                    </div>
                </div>
            </section>

            <section class="product-list section-gallery section-badge ">
                <div class="containerx">
                    <div class="align-center mt-50">
                        <h2>Accessories</h2>

                    </div>
                    <div class=" mt-30">
                        <div class="swiper-container mySwiper">
                            <div class="swiper-wrapper">
                                <?php 
                                for ($i=0; $i < 9; $i++) { 
                                    # code...
                                ?>
                                <div class="swiper-slide">
                                    <div class="gallery-item">
                                        <div class="gallery-content">
                                            <span class="badge">Marine</span>
                                            <h3>Accessories of Suzuki Marine</h3>
                                        </div>
                                        <img src="https://i.ibb.co/fQjW7sL/image-63.png" class="w-100" alt="">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>

                </div>
            </section>





        </main>
        <script>
        var type = 'marine';
        </script>
        <footer id="footer">
            <div class="container">
                <div class="footer-nav">
                    <div class="nav-section">
                        <h3>Produk &amp; Layanan</h3>
                        <ul>
                            <li><a href="https://www.suzuki.co.id/automobile">Mobil Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/motorcycle">Motor Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/marine">Marine Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/pricelist">Daftar Harga Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/dealers">Temukan Dealer Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/services">Servis Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/eparts">Suku Cadang Suzuki</a></li>
                            <li><a href="https://dms.suzuki.co.id/simdms/assets/custom/productQualityUpdate.cshtml">Suzuki
                                    Quality Update</a></li>
                        </ul>
                    </div>
                    <div class="nav-section">
                        <h3>Pojok Suzuki</h3>
                        <ul>
                            <li><a href="https://www.suzuki.co.id/club/car">Car Club</a></li>
                            <li><a href="https://www.suzuki.co.id/club/motor">Motor Club</a></li>
                            <li><a href="https://www.suzuki.co.id/promo">Promo</a></li>
                            <li><a href="https://www.suzuki.co.id/news">Press Release</a></li>
                            <li><a href="https://www.suzuki.co.id/tips-trik">Tips and Trick</a></li>
                            <li><a href="https://www.suzuki.co.id/mysuzukistory">My Suzuki Story</a></li>
                            <li><a href="http://bikerscommunitysuzuki.com">Bikers Community</a></li>
                            <li><a href="https://suzukigsxseries.com">GSX Series</a></li>
                        </ul>
                    </div>
                    <div class="nav-section">
                        <h3>Lainnya</h3>
                        <ul>
                            <li><a href="https://www.suzuki.co.id/contact">Hubungi Kami</a></li>
                            <li><a href="http://www.sfi.co.id/">Suzuki Finance Indonesia</a></li>
                            <li><a href="http://www.autovalue.co.id/">Suzuki Autovalue</a></li>
                            <li><a href="https://www.suzuki.co.id/suzuki-insurance">Suzuki Insurance</a></li>
                        </ul>
                    </div>
                    <div class="nav-section">
                        <h3>Suzuki Corporate</h3>
                        <ul>
                            <li><a href="https://www.suzuki.co.id/corporate/tentang-suzuki">Tentang Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/corporate/sejarah">Sejarah</a></li>
                            <li><a href="https://www.suzuki.co.id/kunjungan-pabrik">Kunjungan Pabrik</a></li>
                            <li><a href="https://www.suzuki.co.id/corporate/penghargaan">Penghargaan</a></li>
                            <li><a href="https://www.suzuki.co.id/corporate/karir">Karir</a></li>
                            <li><a href="http://www.globalsuzuki.com/">Suzuki Global</a></li>
                            <li><a href="https://www.suzuki.co.id/corporate/csr">CSR</a></li>
                        </ul>
                    </div>
                    <div class="nav-section">
                        <h3>Hubungi Kami</h3>
                        <p class="fs-20 mb-5">Halo Suzuki<br><strong><a
                                    href="tel:0800-1100-800">0800-1100-800</a></strong></p>
                        <p class="fs-12 text-muted">Bebas Pulsa, 24 Jam Siaga Melayani Anda</p>
                        <a href="https://play.google.com/store/apps/details?id=com.sim.mysuzuki" target="_blank">
                            <span>Download My Suzuki</span>
                            <img alt="" src="https://suzukicdn.net/themes/default2019/img/mysuzuki.png" alt="">

                        </a>
                        <a href="https://www.suzuki.co.id/ecstar" target="_blank">
                            <img alt="" src="https://suzukicdn.net/themes/default2019/img/ecstar3.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="footer-nav row">
                    <div class="col-6">
                        <h3>suzuki mobil</h3>
                        <p class="fs-24">
                            <a href="https://id-id.facebook.com/suzukiindonesia" target="_blank" class="mr-5">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a href="https://twitter.com/suzukiindonesia" target="_blank" class="mr-5">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="https://www.instagram.com/suzuki_id" target="_blank" class="mr-5">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a href="https://www.youtube.com/user/SuzukiID" target="_blank" class="mr-5">
                                <i class="fa fa-youtube"></i>
                            </a>
                        </p>
                    </div>
                    <div class="col-6">
                        <h3>suzuki motor</h3>
                        <p class="fs-24">
                            <a href="https://id-id.facebook.com/SuzukiMotorcyclesID" target="_blank" class="mr-5">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a href="https://twitter.com/suzukimotorid" target="_blank" class="mr-5">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="https://www.instagram.com/suzukiindonesiamotor" target="_blank" class="mr-5">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a href="https://www.youtube.com/suzukimotorcyclesindonesia" target="_blank" class="mr-5">
                                <i class="fa fa-youtube"></i>
                            </a>
                        </p>
                    </div>
                </div>
                <div class="copyright scbottom">© 2019 Suzuki Indonesia. All rights reserved.</div>
            </div>
        </footer>
    </div>
    <script src="https://suzukicdn.net/themes/default2019/js/aos.js"></script>
    <script src="https://suzukicdn.net/themes/default2019/js/owl.carousel.min.js"></script>
    <script src="https://suzukicdn.net/themes/default2019/js/app.min.js?v=1.1.25"></script>
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>

    <script src="https://suzukicdn.net/themes/default2019/js/bootstrap.min.js"></script>
    <script src="https://suzukicdn.net/themes/default2019/js/marine.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script>
    $('.feature-description div:first-child').show();
    $('.feature-icons a:first-child').addClass('active');
    $('.feature-icons a').click(function() {
        var target = $(this).attr('data-target');
        $(target).siblings().hide();
        $(target).fadeIn();
        $(this).siblings('.active').removeClass('active');
        $(this).addClass('active');
    })
    </script>
    <script>
    var swiper = new Swiper(".mySwiper", {
        slidesPerView: 3,
        spaceBetween: 10,
        autoplay: {
            delay: 7500,
            disableOnInteraction: false,
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
            dynamicBullets: true,
        },
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 40,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 50,
            },
        },
    });

    // Tabs JS
    const btns = document.querySelectorAll(".tabs__button");
    const tabContent = document.querySelectorAll(".tab-content");

    for (let i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", () => {
            addClassFunc(btns[i], "tabs__button--active");
            clearClassFunc(i, btns, "tabs__button--active");

            addClassFunc(tabContent[i], "tab-content--active");
            clearClassFunc(i, tabContent, "tab-content--active");
        });
    }

    function addClassFunc(elem, elemClass) {
        elem.classList.add(elemClass);
    }

    function clearClassFunc(indx, elems, elemClass) {
        for (let i = 0; i < elems.length; i++) {
            if (i === indx) {
                continue;
            }
            elems[i].classList.remove(elemClass);
        }
    }
    </script>
    <!-- <script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=1159525&mt_adid=185618&s1=https://www.suzuki.co.id/&s2=&s3=indonesia'></script> -->
</body>

</html>