<?php
require_once 'config.php';
require_once ROOT_PATH.'/lib/dao_utility.php';
require_once ROOT_PATH.'/lib/mysqlDao.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--[if IE]><link rel="icon" href="https://suzukicdn.net/themes/mobile2019//favicon.ico"><![endif]-->
    <title>Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia</title>
    <meta name="description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="google-site-verification" content="qStaE60pis5fkmA_6n4pOgeqXgvmFiws7fYMCkxL5Fc" />

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta property="og:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta property="og:url" content="https://www.suzuki.co.id/marine">
    <meta property="og:image" content="https://suzukicdn.net/themes/mobile2019/img/logo.png?1.1.19">
    <meta property="og:site_name" content="Suzuki Indonesia">
    <meta property="og:see_also" content="https://twitter.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.facebook.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.instagram.com/suzuki_id">

    <meta name="twitter:site" content="https://www.suzuki.co.id/marine">
    <meta name="twitter:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta name="twitter:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="twitter:image" content="https://suzukicdn.net/themes/mobile2019/img/logo.png?1.1.19">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:creator" content="@Suzukiindonesia">
    <link rel="canonical" href="https://www.suzuki.co.id/marine">

    <link href="https://suzukicdn.net/themes/mobile2019/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/mobile2019/css/aos.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/mobile2019/css/reset.min.css?v=1.1.19" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/mobile2019/css/style.min.css?v=1.1.19" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="32x32"
        href="https://suzukicdn.net/themes/mobile2019/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16"
        href="https://suzukicdn.net/themes/mobile2019/icons/favicon-16x16.png">
    <!-- <link rel="manifest" href="https://suzukicdn.net/themes/mobile2019//manifest.json"> -->
    <meta name="theme-color" content="#173e81">
    <meta name="apple-mobile-web-app-capable" content="no">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="apple-mobile-web-app-title" content="Suzuki Indonesia">
    <link rel="apple-touch-icon" href="https://suzukicdn.net/themes/mobile2019/icons/apple-touch-icon-152x152.png">
    <meta name="msapplication-TileImage"
        content="https://suzukicdn.net/themes/mobile2019/icons/msapplication-icon-144x144.png">
    <meta name="msapplication-TileColor" content="#173e81">
    <!-- <link href="<?php echo ROOT_URL?>/assets/themes/default2019/css/newMarineDefault2019.css?<?php echo rand()?>"
        rel="stylesheet"> -->
    <link href="<?php echo ROOT_URL?>/assets/themes/mobile2019/css/newMarineMobile2019.css?<?php echo rand()?>"
        rel="stylesheet">

    <script type="text/javascript">
    var base_url = 'https://www.suzuki.co.id/';
    </script>
    <script src="https://suzukicdn.net/themes/mobile2019/js/jquery.min.js"></script>



</head>

<body>

    <div id="app">
        <?php
            require_once 'include/m_header.php';
       ?>
        <main id="product" class="mt-0 product-marine newMarineMobile2019">

            <!-- Hero -->
            <section class="headline headline-mobile page-gallery pt-100 pb-50">
                <div class="container">
                    <img alt="" src="https://i.ibb.co/xDRSqw6/image-749.png" class="mt-100" style="width:50px">
                    <h2 class="mb-10">List Product</h2>
                    <p class="mb-30 mt-30">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Turpis ipsum nisl
                        iaculis et sit. Donec porttitor sagittis.</p>
                </div>
            </section>

            <section class="section-tab section-tab-technology">
                <div class="tab-box">
                    <div class="tabs">
                        <button class="tabs__button tabs__button--active" type="button">DURABILITY</button>
                        <button class="tabs__button" type="button">PERFORMANCE</button>
                        <button class="tabs__button" type="button">EASY & COMFORT</button>
                        <button class="tabs__button" type="button">ECONOMY</button>
                    </div>

                    <div class="tab-content tab-content--active">
                        <section class="section-gallery section-engine section-technology">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="tab-image">
                                            <img src="https://i.ibb.co/VwVvcYN/Group-829.png" alt="">
                                            <h1>01</h1>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="tab-text">
                                            <h3>SUZUKI DUAL LOUVER SYSTEM</h3>
                                            <p>The DF350A/DF325A/DF300B is equipped with a dog-leg shaped dual louver at
                                                the air intake to completely remove water from the air taken into the
                                                cowl.</p>
                                            <br>
                                            <p><strong>ADVANTAGE</strong></p>
                                            <p>Prevents water from entering the outboard.
                                                Allows a direct intake system, contributing to higher engine output.</p>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="tab-content">
                        <section class="section-gallery section-engine section-technology">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="tab-image">
                                            <img src="https://i.ibb.co/VwVvcYN/Group-829.png" alt="">
                                            <h1>02</h1>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="tab-text">
                                            <h3>PERFORMANCE</h3>
                                            <p>The DF350A/DF325A/DF300B is equipped with a dog-leg shaped dual louver at
                                                the air intake to completely remove water from the air taken into the
                                                cowl.</p>
                                            <br>
                                            <p><strong>ADVANTAGE</strong></p>
                                            <p>Prevents water from entering the outboard.
                                                Allows a direct intake system, contributing to higher engine output.</p>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="tab-content">
                        <section class="section-gallery section-engine section-technology">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="tab-image">
                                            <img src="https://i.ibb.co/VwVvcYN/Group-829.png" alt="">
                                            <h1>03</h1>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="tab-text">
                                            <h3>EASY & COMFORT</h3>
                                            <p>The DF350A/DF325A/DF300B is equipped with a dog-leg shaped dual louver at
                                                the air intake to completely remove water from the air taken into the
                                                cowl.</p>
                                            <br>
                                            <p><strong>ADVANTAGE</strong></p>
                                            <p>Prevents water from entering the outboard.
                                                Allows a direct intake system, contributing to higher engine output.</p>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="tab-content">
                        <section class="section-gallery section-engine section-technology">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="tab-image">
                                            <img src="https://i.ibb.co/VwVvcYN/Group-829.png" alt="">
                                            <h1>04</h1>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="tab-text">
                                            <h3>ECONOMY</h3>
                                            <p>The DF350A/DF325A/DF300B is equipped with a dog-leg shaped dual louver at
                                                the air intake to completely remove water from the air taken into the
                                                cowl.</p>
                                            <br>
                                            <p><strong>ADVANTAGE</strong></p>
                                            <p>Prevents water from entering the outboard.
                                                Allows a direct intake system, contributing to higher engine output.</p>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </section>

            <section class="hero hero-product section-technology">
                <img src="<?php echo ROOT_URL?>/assets/themes/mobile2019/img/technology/img1.jpg" alt="" class="w-100">
                <div class="caption caption-bottom align-center" style="padding:0;">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <h3 class="mb-10">DUAL WATER INLET</h3>
                                <p>The engine’s cooling system relies on water supplied through two water<br>
                                    inlets located on the lower unit where usually there is only one.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



            <section class="hero hero-product hero-text-right">
                <img src="<?php echo ROOT_URL?>/assets/themes/mobile2019/img/technology/img2.jpg" alt="" class="w-100">
                <div class="caption caption-middle">
                    <div class="container">
                        <div class="row">
                            <div class="col-12" style="text-align:start;">
                                <h3 class="mb-10">SELF-ADJUSTING <br> TIMING CHAIN
                                </h3>
                                <p>The timing chain runs in an oil-bath so it never needs lubricating, and is equipped
                                    with an automatic hydraulic tensioner so it remains properly adjusted at all times.
                                    <br>
                                    <br>
                                    <strong>ADVANTAGE</strong>
                                    <br>
                                    More durability compared to belt types of same class.<br>
                                    Maintenance-free.
                                </p>
                            </div>
                            <div class="col-12">

                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="product-list section-news section bg-darkblue">
                <div class="container">
                    <div class="align-center mt-50">
                        <h2 class="text-white">Suzuki Marine
                            Performance Technology</h2>
                        <p class="text-white">Hard to break | Notice user before engine trouble | Easy to service,
                            repair
                        </p>
                    </div>
                    <div class="product-list-marine cardUXFeature mt-50">
                        <div class="swiper-container mySwiper">
                            <div class="swiper-wrapper">
                                <?php 
                                for ($i=1; $i < 3; $i++) { 
                                    # code...
                                ?>
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img src="https://i.ibb.co/1rHq863/image-35.png" alt="Avatar"
                                            style="width:100%">
                                        <div class="card-body">
                                            <div class="card-text">
                                                <span class="dateNews">June 01/ 2019</span>
                                                <h5><?php echo $i ?>. Clean-Up the World Campaign</h5>
                                                <p>More than 8,000 people from 26 Suzuki distributors participated in
                                                    this activity.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>

                </div>
            </section>

            <section class="hero hero-product ">
                <img src="<?php echo ROOT_URL?>/assets/themes/mobile2019/img/technology/img3.jpg" alt="" class="w-100">
            </section>

        </main>
        <?php 
            require_once 'include/m_footer.php';
        ?>
    </div>

    <div class="floating-menu">
        <a href="https://www.suzuki.co.id/pricelist" target="_blank" class='flbtnpricelist'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-pricelist.png" class='flbtnpricelist'>
            Price List
        </a>
        <a href="https://www.suzuki.co.id/dealers?t=marine" target="_blank" class='flbtndealer'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-dealer.png" class='flbtndealer'>
            Dealer
        </a>
        <a href="https://www.suzuki.co.id/services/halo-suzuki" target="_blank" class='flbtnhalo'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-phone.png" class='flbtnhalo'>
            Halo Suzuki
        </a>

        <a href="https://www.suzuki.co.id/test-drive" target="_blank" class='flbtntestdrive'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-drive.png" class='flbtntestdrive'>
            Drive/Ride
        </a>


    </div>

    <script src="https://suzukicdn.net/themes/mobile2019/js/aos.js"></script>
    <script src="https://suzukicdn.net/themes/mobile2019/js/app.min.js?v=1.1.19"></script>
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>

    <script src="https://suzukicdn.net/themes/mobile2019/js/bootstrap.min.js"></script>
    <script src="https://suzukicdn.net/themes/mobile2019/js/marine.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <script>
    var swiper = new Swiper(".mySwiper", {
        slidesPerView: 1,
        spaceBetween: 10,
        slideToClickedSlide: true,
        loop: true,
        centeredSlides: true,
        pagination: {
            el: ".swiper-pagination",
            dynamicBullets: true,
        },
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 40,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 50,
            },
        },
    });
    // Tabs JS
    const btns = document.querySelectorAll(".tabs__button");
    const tabContent = document.querySelectorAll(".tab-content");

    for (let i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", () => {
            addClassFunc(btns[i], "tabs__button--active");
            clearClassFunc(i, btns, "tabs__button--active");

            addClassFunc(tabContent[i], "tab-content--active");
            clearClassFunc(i, tabContent, "tab-content--active");
        });
    }

    function addClassFunc(elem, elemClass) {
        elem.classList.add(elemClass);
    }

    function clearClassFunc(indx, elems, elemClass) {
        for (let i = 0; i < elems.length; i++) {
            if (i === indx) {
                continue;
            }
            elems[i].classList.remove(elemClass);
        }
    }
    </script>

    <!-- end webpushr tracking code -->
</body>

</html>