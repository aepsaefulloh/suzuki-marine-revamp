<?php
require_once 'config.php';
require_once ROOT_PATH.'/lib/dao_utility.php';
require_once ROOT_PATH.'/lib/mysqlDao.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--[if IE]><link rel="icon" href="https://suzukicdn.net/themes/mobile2019//favicon.ico"><![endif]-->
    <title>Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia</title>
    <meta name="description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="google-site-verification" content="qStaE60pis5fkmA_6n4pOgeqXgvmFiws7fYMCkxL5Fc" />

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta property="og:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta property="og:url" content="https://www.suzuki.co.id/marine">
    <meta property="og:image" content="https://suzukicdn.net/themes/mobile2019/img/logo.png?1.1.19">
    <meta property="og:site_name" content="Suzuki Indonesia">
    <meta property="og:see_also" content="https://twitter.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.facebook.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.instagram.com/suzuki_id">

    <meta name="twitter:site" content="https://www.suzuki.co.id/marine">
    <meta name="twitter:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta name="twitter:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="twitter:image" content="https://suzukicdn.net/themes/mobile2019/img/logo.png?1.1.19">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:creator" content="@Suzukiindonesia">
    <link rel="canonical" href="https://www.suzuki.co.id/marine">

    <link href="https://suzukicdn.net/themes/mobile2019/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/mobile2019/css/aos.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/mobile2019/css/reset.min.css?v=1.1.19" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/mobile2019/css/style.min.css?v=1.1.19" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="32x32"
        href="https://suzukicdn.net/themes/mobile2019/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16"
        href="https://suzukicdn.net/themes/mobile2019/icons/favicon-16x16.png">
    <!-- <link rel="manifest" href="https://suzukicdn.net/themes/mobile2019//manifest.json"> -->
    <meta name="theme-color" content="#173e81">
    <meta name="apple-mobile-web-app-capable" content="no">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="apple-mobile-web-app-title" content="Suzuki Indonesia">
    <link rel="apple-touch-icon" href="https://suzukicdn.net/themes/mobile2019/icons/apple-touch-icon-152x152.png">
    <meta name="msapplication-TileImage"
        content="https://suzukicdn.net/themes/mobile2019/icons/msapplication-icon-144x144.png">
    <meta name="msapplication-TileColor" content="#173e81">
    <!-- <link href="<?php echo ROOT_URL?>/assets/themes/default2019/css/newMarineDefault2019.css?<?php echo rand()?>"
        rel="stylesheet"> -->
    <link href="<?php echo ROOT_URL?>/assets/themes/mobile2019/css/newMarineMobile2019.css?<?php echo rand()?>"
        rel="stylesheet">

    <script type="text/javascript">
    var base_url = 'https://www.suzuki.co.id/';
    </script>
    <script src="https://suzukicdn.net/themes/mobile2019/js/jquery.min.js"></script>



</head>

<body>

    <div id="app">
        <?php
            require_once 'include/m_header.php';
       ?>
        <main id="product" class="mt-0 product-marine newMarineMobile2019">

            <!-- Hero -->
            <section class="headline headline-mobile page-gallery pt-100 pb-50">
                <div class="container">
                    <img alt="" src="https://i.ibb.co/xDRSqw6/image-749.png" class="mt-100" style="width:50px">
                    <h2 class="mb-10">Gallery</h2>
                    <p class="mb-30 mt-30">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Turpis ipsum nisl
                        iaculis et sit. Donec porttitor sagittis.</p>
                </div>
            </section>

            <section class="product-list section-gallery bg-white">
                <div class="container-fluid">
                    <div class="align-center">
                        <h2>Galeri</h2>
                        <div class="row">
                            <?php 
                                for ($i=0; $i < 6; $i++) { 
                                    # code...
                                
                            ?>
                            <div class="col-6">
                                <div class="gallery-item">
                                    <div class="gallery-content">
                                        <h3>DF175A/DF150A </h3>
                                        <h3>OFFICIAL PROMOTIONAL VIDEO</h3>
                                    </div>
                                    <img src="https://i.ibb.co/fQjW7sL/image-63.png" class="w-100" alt="">
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="pagination mt-50">

                        <div class="page">
                            <div class="paging paging-marine">
                                <a href="javascript:void(0)" class="active">1</a>
                                <a href="javascript:void(0)">2</a>
                                <a href="javascript:void(0)">3</a>
                                <a href="javascript:void(0)" rel="next">&gt;</a>
                                <a href="javascript:void(0)">»</a>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <section class="product-list section-gallery section-badge bg-white">
                <div class="containerx">
                    <div class="align-center">
                        <h2>Accessories</h2>

                    </div>
                    <div class=" mt-30">
                        <div class="swiper-container mySwiper swiper-accessories">
                            <div class="swiper-wrapper">
                                <?php 
                                for ($i=0; $i < 9; $i++) { 
                                    # code...
                                ?>
                                <div class="swiper-slide">
                                    <div class="gallery-item">
                                        <div class="gallery-content">
                                            <span class="badge">Marine</span>
                                            <h3>Accessories of Suzuki Marine</h3>
                                        </div>
                                        <img src="https://i.ibb.co/fQjW7sL/image-63.png" class="w-100" alt="">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>

                </div>
            </section>


        </main>
        <?php 
            require_once 'include/m_footer.php';
        ?>
    </div>

    <div class="floating-menu">
        <a href="https://www.suzuki.co.id/pricelist" target="_blank" class='flbtnpricelist'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-pricelist.png" class='flbtnpricelist'>
            Price List
        </a>
        <a href="https://www.suzuki.co.id/dealers?t=marine" target="_blank" class='flbtndealer'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-dealer.png" class='flbtndealer'>
            Dealer
        </a>
        <a href="https://www.suzuki.co.id/services/halo-suzuki" target="_blank" class='flbtnhalo'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-phone.png" class='flbtnhalo'>
            Halo Suzuki
        </a>

        <a href="https://www.suzuki.co.id/test-drive" target="_blank" class='flbtntestdrive'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-drive.png" class='flbtntestdrive'>
            Drive/Ride
        </a>


    </div>

    <script src="https://suzukicdn.net/themes/mobile2019/js/aos.js"></script>
    <script src="https://suzukicdn.net/themes/mobile2019/js/app.min.js?v=1.1.19"></script>
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>

    <script src="https://suzukicdn.net/themes/mobile2019/js/bootstrap.min.js"></script>
    <script src="https://suzukicdn.net/themes/mobile2019/js/marine.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <script>
    var swiper = new Swiper(".mySwiper.swiper-accessories", {
        slidesPerView: 1,
        spaceBetween: 10,
        slideToClickedSlide: true,
        loop: true,
        centeredSlides: true,
        pagination: {
            el: ".swiper-pagination",
            dynamicBullets: true,
        },
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 40,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 50,
            },
        },
    });
    // Ta
    </script>

    <!-- end webpushr tracking code -->
</body>

</html>