<?php
require_once 'config.php';
require_once ROOT_PATH.'/lib/dao_utility.php';
require_once ROOT_PATH.'/lib/mysqlDao.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--[if IE]><link rel="icon" href="https://suzukicdn.net/themes/mobile2019//favicon.ico"><![endif]-->
    <title>Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia</title>
    <meta name="description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="google-site-verification" content="qStaE60pis5fkmA_6n4pOgeqXgvmFiws7fYMCkxL5Fc" />

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta property="og:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta property="og:url" content="https://www.suzuki.co.id/marine">
    <meta property="og:image" content="https://suzukicdn.net/themes/mobile2019/img/logo.png?1.1.19">
    <meta property="og:site_name" content="Suzuki Indonesia">
    <meta property="og:see_also" content="https://twitter.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.facebook.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.instagram.com/suzuki_id">

    <meta name="twitter:site" content="https://www.suzuki.co.id/marine">
    <meta name="twitter:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta name="twitter:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="twitter:image" content="https://suzukicdn.net/themes/mobile2019/img/logo.png?1.1.19">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:creator" content="@Suzukiindonesia">
    <link rel="canonical" href="https://www.suzuki.co.id/marine">

    <link href="https://suzukicdn.net/themes/mobile2019/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/mobile2019/css/aos.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/mobile2019/css/reset.min.css?v=1.1.19" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/mobile2019/css/style.min.css?v=1.1.19" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="32x32"
        href="https://suzukicdn.net/themes/mobile2019/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16"
        href="https://suzukicdn.net/themes/mobile2019/icons/favicon-16x16.png">
    <!-- <link rel="manifest" href="https://suzukicdn.net/themes/mobile2019//manifest.json"> -->
    <meta name="theme-color" content="#173e81">
    <meta name="apple-mobile-web-app-capable" content="no">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="apple-mobile-web-app-title" content="Suzuki Indonesia">
    <link rel="apple-touch-icon" href="https://suzukicdn.net/themes/mobile2019/icons/apple-touch-icon-152x152.png">
    <meta name="msapplication-TileImage"
        content="https://suzukicdn.net/themes/mobile2019/icons/msapplication-icon-144x144.png">
    <meta name="msapplication-TileColor" content="#173e81">
    <!-- <link href="<?php echo ROOT_URL?>/assets/themes/default2019/css/newMarineDefault2019.css?<?php echo rand()?>"
        rel="stylesheet"> -->
    <link href="<?php echo ROOT_URL?>/assets/themes/mobile2019/css/newMarineMobile2019.css?<?php echo rand()?>"
        rel="stylesheet">

    <script type="text/javascript">
    var base_url = 'https://www.suzuki.co.id/';
    </script>
    <script src="https://suzukicdn.net/themes/mobile2019/js/jquery.min.js"></script>



</head>

<body>

    <div id="app">
        <?php
            require_once 'include/m_header.php';
       ?>
        <main id="product" class="mt-0 product-marine newMarineMobile2019 blank">

            <!-- Hero -->
            <section class="headline headline-mobile page-detail-medium pt-100 pb-50">
                <div class="container">
                    <h2 class="mb-10 pt-100">Suzuki DF115A</h2>
                    <p class="mb-30 mt-30">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Turpis ipsum nisl
                        iaculis et sit. Donec porttitor sagittis.</p>
                    <img alt="" src="http://52.77.68.225/themes/default2019/img/marine/logo-ultimate.png" class=""
                        style="width:100px"><br>
                    <img alt="" src="https://i.ibb.co/xDRSqw6/image-749.png" class="" style="width:50px">

                    <img alt="" src="<?php echo ROOT_URL?>/assets/themes/mobile2019/img/detail/medium/hero-md.png"
                        class="w-100">
                </div>
            </section>

            <div class="tab-box tab-detail-md">
                <div class="tabs" style="background:#1071B8;">
                    <button class="tabs__button tabs__button--active" type="button">Feature</button>
                    <button class="tabs__button" type="button">Technology</button>
                </div>

                <div class="tab-content tab-content--active ">
                    <section class="product-list section-gallery section-engine section-detail-md-tab">
                        <div class="container">
                            <div class="topHeading" style="text-align:start;">
                                <h2 class="text-white">Maximizing Fuel Economy and Performance</h2>
                                <p class="text-white">With the launch of the new generation DF140A, DF115A and DF100A,
                                    our engineers have delivered a trio of outboards that offer great fuel economy
                                    without sacrificing on performance. This has been achieved by employing our advanced
                                    Lean Burn Fuel Control System.
                                </p>
                                <div class="num right">
                                    <h1>1</h1>
                                </div>
                            </div>
                            <div class="line1 mt-50 mb-50"></div>
                            <div class="topHeading" style="text-align:end;">
                                <h2 class="text-white">Transferring Power Into Speed</h2>
                                <p class="text-white">Our in-line four cylinder outboards utilise aggressive gear ratios
                                    that enable these outboards to swing a larger prop. The combination of a large prop
                                    and lower gear ratio delivers plenty of torque, exhilarating acceleration and
                                    exciting top-end speed.
                                </p>
                                <div class="num left">
                                    <h1>2</h1>
                                </div>
                            </div>
                            <div class="line1 mt-50 mb-50"></div>
                            <div class="topHeading" style="text-align:start;">
                                <h2 class="text-white">All Torque</h2>
                                <p class="text-white">With acceleration and manoeuvrability high on the list of
                                    priorities it is no surprise that the DF140, with its excellent power to weight
                                    ratio, was one of the models selected for use at the London 2012 Olympic &
                                    Paralympic Games Sailing Regatta.
                                </p>
                                <div class="num right">
                                    <h1>3</h1>
                                </div>
                            </div>
                            <div class="line1 mt-50 mb-50"></div>
                            <div class="topHeading" style="text-align:end;">
                                <h2 class="text-white">Innovative Technology</h2>
                                <p class="text-white">The DF115A include an O2 Sensor Feedback System that helps to keep
                                    emissions cleaner and more stable. In addition to this the DF140A and DF115A also
                                    benefit form a knock sensor that detects and controls abnormal combustion allowing
                                    the engine to operate at optimum performance.
                                </p>
                                <div class="num left">
                                    <h1>4</h1>
                                </div>
                            </div>
                            <div class="line1 mt-50 mb-50"></div>
                            <div class="topHeading" style="text-align:start;">
                                <h2 class="text-white">Cleaner, More Efficient Operation</h2>
                                <p class="text-white">Suzuki utilizes a number of its advanced technologies to deliver
                                    cleaner, more efficient operation that conforms to the Recreational Craft Directive
                                    (RCD) – Directive 2003/44EC of the European Parliament and of the Council, and has
                                    received three-star ratings from the California Air Resources Board (CARB).
                                </p>
                                <img src="<?php echo ROOT_URL?>/assets/themes/mobile2019/img/detail/medium/star.png"
                                    alt="">
                                <div class="num right">
                                    <h1>5</h1>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="tab-content">
                    <section class="product-list section-gallery section-engine section-detail-md-tab">
                        <div class="container">
                            <div class="topHeading" style="text-align:start;">
                                <h2 class="text-white">Maximizing Fuel Economy and Performance</h2>
                                <p class="text-white">With the launch of the new generation DF140A, DF115A and DF100A,
                                    our engineers have delivered a trio of outboards that offer great fuel economy
                                    without sacrificing on performance. This has been achieved by employing our advanced
                                    Lean Burn Fuel Control System.
                                </p>
                                <div class="num right">
                                    <h1>1</h1>
                                </div>
                            </div>
                            <div class="line1 mt-50 mb-50"></div>
                            <div class="topHeading" style="text-align:end;">
                                <h2 class="text-white">Transferring Power Into Speed</h2>
                                <p class="text-white">Our in-line four cylinder outboards utilise aggressive gear ratios
                                    that enable these outboards to swing a larger prop. The combination of a large prop
                                    and lower gear ratio delivers plenty of torque, exhilarating acceleration and
                                    exciting top-end speed.
                                </p>
                                <div class="num left">
                                    <h1>2</h1>
                                </div>
                            </div>
                            <div class="line1 mt-50 mb-50"></div>
                            <div class="topHeading" style="text-align:start;">
                                <h2 class="text-white">All Torque</h2>
                                <p class="text-white">With acceleration and manoeuvrability high on the list of
                                    priorities it is no surprise that the DF140, with its excellent power to weight
                                    ratio, was one of the models selected for use at the London 2012 Olympic &
                                    Paralympic Games Sailing Regatta.
                                </p>
                                <div class="num right">
                                    <h1>3</h1>
                                </div>
                            </div>
                            <div class="line1 mt-50 mb-50"></div>
                            <div class="topHeading" style="text-align:end;">
                                <h2 class="text-white">Innovative Technology</h2>
                                <p class="text-white">The DF115A include an O2 Sensor Feedback System that helps to keep
                                    emissions cleaner and more stable. In addition to this the DF140A and DF115A also
                                    benefit form a knock sensor that detects and controls abnormal combustion allowing
                                    the engine to operate at optimum performance.
                                </p>
                                <div class="num left">
                                    <h1>4</h1>
                                </div>
                            </div>
                            <div class="line1 mt-50 mb-50"></div>
                            <div class="topHeading" style="text-align:start;">
                                <h2 class="text-white">Cleaner, More Efficient Operation</h2>
                                <p class="text-white">Suzuki utilizes a number of its advanced technologies to deliver
                                    cleaner, more efficient operation that conforms to the Recreational Craft Directive
                                    (RCD) – Directive 2003/44EC of the European Parliament and of the Council, and has
                                    received three-star ratings from the California Air Resources Board (CARB).
                                </p>
                                <img src="<?php echo ROOT_URL?>/assets/themes/mobile2019/img/detail/medium/star.png"
                                    alt="">
                                <div class="num right">
                                    <h1>5</h1>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

            <section class="product-prices">
                <div class="container-fluid">
                    <div class="align-center mt-50">
                        <h2>Daftar Harga</h2>
                        <p>Harga On The Road Jakarta Mei 2021<br>
                            <small>*Harga tidak mengikat, dapat berubah sewaktu-waktu</small>
                        </p>
                        <div class="mt-30 mb-30">
                            <div class="group-model">DF 325 ATX</div>
                            <div class="price">Rp. 404.800.000</div>
                        </div>
                        <div class="group-button">
                            <a href="" class="btn btn-outline-black">Unduh Brosur</a>
                            <a href="" class="btn btn-outline-black">Spesifikasi</a>
                        </div>
                    </div>
                </div>
            </section>

            <section class="product-list section-gallery section-badge ">
                <div class="containerx">
                    <div class="align-center">
                        <h2>Accessories</h2>

                    </div>
                    <div class=" mt-30">
                        <div class="swiper-container mySwiper swiper-accessories">
                            <div class="swiper-wrapper">
                                <?php 
                                for ($i=0; $i < 9; $i++) { 
                                    # code...
                                ?>
                                <div class="swiper-slide">
                                    <div class="gallery-item">
                                        <div class="gallery-content">
                                            <span class="badge">Marine</span>
                                            <h3>Accessories of Suzuki Marine</h3>
                                        </div>
                                        <img src="https://i.ibb.co/fQjW7sL/image-63.png" class="w-100" alt="">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>

                </div>
            </section>
        </main>
        <?php 
            require_once 'include/m_footer.php';
        ?>
    </div>

    <div class="floating-menu">
        <a href="https://www.suzuki.co.id/pricelist" target="_blank" class='flbtnpricelist'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-pricelist.png" class='flbtnpricelist'>
            Price List
        </a>
        <a href="https://www.suzuki.co.id/dealers?t=marine" target="_blank" class='flbtndealer'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-dealer.png" class='flbtndealer'>
            Dealer
        </a>
        <a href="https://www.suzuki.co.id/services/halo-suzuki" target="_blank" class='flbtnhalo'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-phone.png" class='flbtnhalo'>
            Halo Suzuki
        </a>

        <a href="https://www.suzuki.co.id/test-drive" target="_blank" class='flbtntestdrive'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-drive.png" class='flbtntestdrive'>
            Drive/Ride
        </a>


    </div>

    <script src="https://suzukicdn.net/themes/mobile2019/js/aos.js"></script>
    <script src="https://suzukicdn.net/themes/mobile2019/js/app.min.js?v=1.1.19"></script>
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>

    <script src="https://suzukicdn.net/themes/mobile2019/js/bootstrap.min.js"></script>
    <script src="https://suzukicdn.net/themes/mobile2019/js/marine.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <script>
    var swiper = new Swiper(".mySwiper.swiper-accessories", {
        slidesPerView: 1,
        spaceBetween: 10,
        slideToClickedSlide: true,
        loop: true,
        centeredSlides: true,
        pagination: {
            el: ".swiper-pagination",
            dynamicBullets: true,
        },
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 40,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 50,
            },
        },
    });
    // Tabs JS
    const btns = document.querySelectorAll(".tabs__button");
    const tabContent = document.querySelectorAll(".tab-content");

    for (let i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", () => {
            addClassFunc(btns[i], "tabs__button--active");
            clearClassFunc(i, btns, "tabs__button--active");

            addClassFunc(tabContent[i], "tab-content--active");
            clearClassFunc(i, tabContent, "tab-content--active");
        });
    }

    function addClassFunc(elem, elemClass) {
        elem.classList.add(elemClass);
    }

    function clearClassFunc(indx, elems, elemClass) {
        for (let i = 0; i < elems.length; i++) {
            if (i === indx) {
                continue;
            }
            elems[i].classList.remove(elemClass);
        }
    }
    </script>

    <!-- end webpushr tracking code -->
</body>

</html>