<footer id="footer">
    <div class="container">
        <div class="footer-nav row">
            <div class="nav-section col-6">
                <h3>Produk &amp; Layanan</h3>
                <ul>
                    <li><a href="https://www.suzuki.co.id/automobile">Mobil Suzuki</a></li>
                    <li><a href="https://www.suzuki.co.id/motorcycle">Motor Suzuki</a></li>
                    <li><a href="https://www.suzuki.co.id/marine">Marine Suzuki</a></li>
                    <li><a href="https://www.suzuki.co.id/pricelist">Daftar Harga Suzuki</a></li>
                    <li><a href="https://www.suzuki.co.id/dealers">Temukan Dealer Suzuki</a></li>
                    <li><a href="https://www.suzuki.co.id/services">Servis Suzuki</a></li>
                    <li><a href="https://www.suzuki.co.id/eparts">Suku Cadang Suzuki</a></li>
                    <li><a href="https://dms.suzuki.co.id/simdms/assets/custom/productQualityUpdate.cshtml">Suzuki
                            Quality Update</a></li>
                </ul>
            </div>
            <div class="nav-section col-6">
                <h3>Suzuki Corporate</h3>
                <ul>
                    <li><a href="https://www.suzuki.co.id/corporate/tentang-suzuki">Tentang Suzuki</a></li>
                    <li><a href="https://www.suzuki.co.id/corporate/sejarah">Sejarah</a></li>
                    <li><a href="https://www.suzuki.co.id/kunjungan-pabrik">Kunjungan Pabrik</a></li>
                    <li><a href="https://www.suzuki.co.id/corporate/penghargaan">Penghargaan</a></li>
                    <li><a href="https://www.suzuki.co.id/corporate/karir">Karir</a></li>
                    <li><a href="http://www.globalsuzuki.com/">Suzuki Global</a></li>
                    <li><a href="https://www.suzuki.co.id/corporate/csr">CSR</a></li>
                    </li>
                </ul>
            </div>
            <div class="nav-section col-6">
                <h3>Pojok Suzuki</h3>
                <ul>
                    <li><a href="https://www.suzuki.co.id/club/car">Car Club</a></li>
                    <li><a href="https://www.suzuki.co.id/club/motor">Motor Club</a></li>
                    <li><a href="https://www.suzuki.co.id/promo">Promo</a></li>
                    <li><a href="https://www.suzuki.co.id/news">Press Release</a></li>
                    <li><a href="https://www.suzuki.co.id/tips-trik">Tips and Trick</a></li>
                    <li><a href="https://www.suzuki.co.id/mysuzukistory">My Suzuki Story</a></li>
                    <li><a href="http://bikerscommunitysuzuki.com">Bikers Community</a></li>
                    <li><a href="https://suzukigsxseries.com">GSX Series</a></li>
                </ul>
            </div>
            <div class="nav-section col-6">
                <h3>Lainnya</h3>
                <ul>
                    <li><a href="https://www.suzuki.co.id/contact">Hubungi Kami</a></li>
                    <li><a href="http://www.sfi.co.id/">Suzuki Finance Indonesia</a></li>
                    <li><a href="http://www.autovalue.co.id/">Suzuki Autovalue</a></li>
                    <li><a href="https://www.suzuki.co.id/suzuki-insurance">Suzuki Insurance</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="footer-nav">
            <div class="nav-section">
                <h3>Hubungi Kami</h3>
                <p class="fs-20 mb-5">Halo Suzuki<br><strong><a href="tel:0800-1100-800">0800-1100-800</a></strong></p>
                <p class="fs-12 text-muted">Bebas Pulsa, 24 Jam Siaga Melayani Anda</p>
                <p class="mt-20 fs-14">
                    <span>Download My Suzuki</span><br>
                    <a href="https://play.google.com/store/apps/details?id=com.sim.mysuzuki" target="_blank"
                        class="d-block mt-10" style='float:left;margin-right:10px'>
                        <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/mysuzuki_new.png" alt="">

                    </a>

                    <a href="https://www.suzuki.co.id/ecstar" target="_blank" class="d-block mt-10">
                        <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/ecstar3.png" alt="">
                    </a>
                </p>

            </div>
        </div>
        <div class="footer-nav row">
            <div class="col-6">
                <h3>suzuki mobil</h3>
                <p class="fs-24">
                    <a href="https://id-id.facebook.com/suzukiindonesia" target="_blank" class="mr-5">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="https://twitter.com/suzukiindonesia" target="_blank" class="mr-5">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="https://www.instagram.com/suzuki_id" target="_blank" class="mr-5">
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a href="https://www.youtube.com/user/SuzukiID" target="_blank" class="mr-5">
                        <i class="fa fa-youtube"></i>
                    </a>
                </p>
            </div>
            <div class="col-6">
                <h3>suzuki motor</h3>
                <p class="fs-24">
                    <a href="https://id-id.facebook.com/SuzukiMotorcyclesID" target="_blank" class="mr-5">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="https://twitter.com/suzukimotorid" target="_blank" class="mr-5">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="https://www.instagram.com/suzukiindonesiamotor" target="_blank" class="mr-5">
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a href="https://www.youtube.com/suzukimotorcyclesindonesia" target="_blank" class="mr-5">
                        <i class="fa fa-youtube"></i>
                    </a>
                </p>
            </div>
        </div>
        <div class="copyright">© 2019 Suzuki Indonesia. All rights reserved.</div>
    </div>
</footer>