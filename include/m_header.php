<header class="">
    <a href="https://www.suzuki.co.id/" class="logo">
        <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/logo-white.png?v=2" class="logo-white"
            style="width: 65px;">
        <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/logo.png?v=2" class="logo-color">
    </a>

    <div style="flex-grow: 1;">
        <ol class="breadcrumb breadcrumb-2 d-none">
            <li><a href="https://www.suzuki.co.id/">Home</a></li>
            <li>Suzuki Marine</li>
        </ol>
    </div>

    <a href="javascript:void(0)" class="menu-toggle" onclick="openNav()">
        <span></span>
        <span></span>
        <span></span>
    </a>
</header>
<nav id="nav">
    <div class="nav-header">
        <div>
            <a href="javascript:void(0)" class="menu-toggle" onclick="closeNav()">
                <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-close.png">
            </a>
        </div>
    </div>

    <div class="nav" id="nav-main">
        <a href="https://www.suzuki.co.id/automobile">Automobile</a>
        <a href="https://www.suzuki.co.id/motorcycle">Motorcycle</a>
        <a href="https://www.suzuki.co.id/marine">Marine</a>
        <a href="https://www.suzuki.co.id/pricelist">Daftar Harga</a>
        <a href="https://www.suzuki.co.id/test-drive/automobile">Test Drive</a>
        <a href="https://www.suzuki.co.id/test-drive/motorcycle">Test Ride</a>
        <hr>
        <a href="javascript:void(0)" onclick="openSubNav('#nav-maintenance')" class="has-sub">Maintenance</a>
        <hr>
        <a href="javascript:void(0)" onclick="openSubNav('#nav-corner')" class="has-sub">Pojok Suzuki</a>
        <hr>
        <a href="javascript:void(0)" onclick="openSubNav('#nav-corporate')" class="has-sub">Korporat</a>
        <a href="https://www.suzuki.co.id/contact">Hubungi Kami</a>
        <hr>
        <a href="https://www.autovalue.co.id/">Suzuki Autovalue <img alt=""
                src="https://suzukicdn.net/themes/mobile2019/img/icon-link.png"></a>
        <a href="https://www.suzuki.co.id/suzuki-insurance">Suzuki Insurance <img alt=""
                src="https://suzukicdn.net/themes/mobile2019/img/icon-link.png"></a>
        <a href="https://www.sfi.co.id/">Suzuki Finance Indonesia <img alt=""
                src="https://suzukicdn.net/themes/mobile2019/img/icon-link.png"></a>
    </div>

    <div class="nav sub-nav" id="nav-maintenance">
        <a href="javascript:void(0)" onclick="closeSubNav()" class="has-sub">Maintenance</a>
        <a href="https://www.suzuki.co.id/service/">Perawatan Berkala</a>
        <a href="https://www.suzuki.co.id/services/booking">Booking Servis</a>
        <a href="https://www.suzuki.co.id/eparts/">Suku Cadang</a>
        <a href="https://www.suzuki.co.id/dealers/">Temukan Dealer</a>
        <a href="https://dms.suzuki.co.id/simdms/assets/custom/ProductQualityUpdate.cshtml">Suzuki Quality
            Update</a>
    </div>

    <div class="nav sub-nav" id="nav-corner">
        <a href="javascript:void(0)" onclick="closeSubNav()" class="has-sub">Pojok Suzuki</a>
        <a href="https://www.suzuki.co.id/club/car/">Car Club</a>
        <a href="https://www.suzuki.co.id/club/motor/">Motor Club</a>
        <a href="https://www.suzuki.co.id/promo/">Promo</a>
        <a href="https://www.suzuki.co.id/news/">Press Release</a>
        <a href="https://www.suzuki.co.id/tips-trik/">Tips dan Trik</a>
        <a href="https://www.suzuki.co.id/mysuzukistory">My Suzuki Story</a>
        <a href="http://bikerscommunitysuzuki.com">Bikers Community</a>
        <a href="https://suzukigsxseries.com">GSX Series</a>
    </div>

    <div class="nav sub-nav" id="nav-corporate">
        <a href="javascript:void(0)" onclick="closeSubNav()" class="has-sub">Korporat</a>
        <a href="https://www.suzuki.co.id/corporate/tentang-suzuki/">Tentang Suzuki</a>
        <a href="https://www.suzuki.co.id/corporate/tentang-gear/">Suzuki Gear</a>
        <a href="https://www.suzuki.co.id/corporate/sejarah/">Sejarah</a>
        <a href="https://www.suzuki.co.id/kunjungan-pabrik/">Kunjungan Pabrik</a>
        <a href="https://www.suzuki.co.id/corporate/penghargaan/">Penghargaan</a>
        <a href="https://www.suzuki.co.id/corporate/karir/">Karir</a>
        <a href="http://www.globalsuzuki.com/">Suzuki Global</a>
        <a href="https://www.suzuki.co.id/corporate/csr/">CSR</a>
        <a href="https://www.suzuki.co.id/privacy-policy/">Privacy Policy</a>
    </div>
</nav>