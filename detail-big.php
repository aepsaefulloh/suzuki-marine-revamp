<?php
require_once 'config.php';
require_once ROOT_PATH.'/lib/dao_utility.php';
require_once ROOT_PATH.'/lib/mysqlDao.php';
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--[if IE]><link rel="icon" href="https://suzukicdn.net/themes/default2019//favicon.ico"><![endif]-->
    <title>Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia</title>
    <meta name="description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="google-site-verification" content="qStaE60pis5fkmA_6n4pOgeqXgvmFiws7fYMCkxL5Fc" />

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta property="og:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta property="og:url" content="https://www.suzuki.co.id/marine">
    <meta property="og:image" content="https://suzukicdn.net/themes/default2019/img/logo.png?1.1.25">
    <meta property="og:site_name" content="Suzuki Indonesia">
    <meta property="og:see_also" content="https://twitter.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.facebook.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.instagram.com/suzuki_id">

    <meta name="twitter:site" content="https://www.suzuki.co.id/marine">
    <meta name="twitter:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta name="twitter:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="twitter:image" content="https://suzukicdn.net/themes/default2019/img/logo.png?1.1.25">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:creator" content="@Suzukiindonesia">
    <link rel="canonical" href="https://www.suzuki.co.id/marine">

    <link href="https://suzukicdn.net/themes/default2019/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/default2019/css/aos.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/default2019/css/reset.min.css?v=1.1.25" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/default2019/css/style.min.css?v=1.1.25" rel="stylesheet">
    <link href="<?php echo ROOT_URL?>/assets/themes/default2019/css/newMarineDefault2019.css?<?php echo rand()?>"
        rel="stylesheet">

    <link rel="icon" type="image/png" sizes="32x32"
        href="https://suzukicdn.net/themes/default2019/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16"
        href="https://suzukicdn.net/themes/default2019/icons/favicon-16x16.png">
    <!-- <link rel="manifest" href="https://suzukicdn.net/themes/default2019//manifest.json"> -->
    <meta name="theme-color" content="#173e81">
    <meta name="apple-mobile-web-app-capable" content="no">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="apple-mobile-web-app-title" content="Suzuki Indonesia">
    <link rel="apple-touch-icon" href="https://suzukicdn.net/themes/default2019/icons/apple-touch-icon-152x152.png">
    <meta name="msapplication-TileImage"
        content="https://suzukicdn.net/themes/default2019/icons/msapplication-icon-144x144.png">
    <meta name="msapplication-TileColor" content="#173e81">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js"></script>
    <script type="text/javascript">
    var base_url = 'https://www.suzuki.co.id/';
    </script>
    <script src="https://suzukicdn.net/themes/default2019/js/jquery.min.js"></script>
</head>

<body>
    <div id="app">
        <?php require_once 'include/header.php'; ?>
        <main id="product" class="mt-0 product-marine newMarine2019 detailnewMarine2019">


            <section class="hero hero-product hero-gallery hero-engine-big">
                <img src="https://i.ibb.co/x6gKjkm/image-706.png" alt="" class="w-100">
                <div class="caption caption-detail">
                    <div class="container">
                        <div class="row ai-center">
                            <div class="col-6">
                                <h2 class="mb-10">Suzuki DF325ATX</h2>
                                <p class="mb-0">The Ultimate Outboard.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <section class="hero hero-product hero-text-right">
                <img src="https://i.ibb.co/XtCj0Tm/main-technology-pc-2.png" alt="" class="w-100">
                <div class="caption caption-middle">
                    <div class="container">
                        <div class="row">
                            <div class="col-7" style="text-align:start;">
                                <h3 class="mb-10">POWER WITH PASSION</h3>
                                <p>We are proud to introduce our DF325A – an outboard that perfectly balances awesome
                                    power and thrust, with outstanding fuel-efficiency and trusted reliability all
                                    within a lightweight and stylish design.
                                    <br>
                                    <br>
                                    Built with the every-day use of larger boats in mind, this market-leading outboard
                                    has been engineered to run on 91 RON fuel and, as a world-first, is the first four
                                    stroke outboard over 300 horsepower to do so.
                                </p>
                            </div>
                            <div class="col-5" style="align-self:center;">
                                <div class="imgColumnCenter">
                                    <img src="<?php echo ROOT_URL?>/assets/img/logo/ultimate.png" class="w-50" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="product-list section-gallery section-feature-engine-big bg-white">
                <div class="container-fluid">
                    <div class="align-center mt-50">
                        <h2>Features Suzuki DF325ATX</h2>
                        <p class="text-muted">FEATURE</p>
                    </div>
                    <div class="product-list-marine cardUXFeature mt-50 mb-50">
                        <div class="swiper-container mySwiper">
                            <div class="swiper-wrapper">
                                <?php 
                                for ($i=1; $i < 5; $i++) { 
                                    # code...
                                ?>
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img src="https://i.ibb.co/1rHq863/image-35.png" alt="Avatar"
                                            style="width:100%">
                                        <div class="card-body">
                                            <div class="card-text">
                                                <h5 class="text-black">91 RON FUEL</h5>
                                                <p style="color: rgba(0, 0, 0, 0.6);">Our engineers set out to
                                                    build a compact, lightweight outboard that
                                                    combines the high power required, alongside the operating
                                                    efficiencies that cannot be achieved by using technologies such as
                                                    turbocharging or supercharging. Additionally, they set the goal of
                                                    making the DF325A run on low octane, 91 RON fuel, which combined
                                                    with legendary Suzuki reliability makes the outboard ideally suited
                                                    for a wide variety of large boats around the world.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <!-- <div class="swiper-pagination"></div> -->
                        </div>
                    </div>
                </div>
            </section>

            <section class="hero hero-product ">
                <img src="https://i.ibb.co/SXDLWhj/main-gallery-pc.png" alt="" class="w-100">
            </section>
            <section class="product-prices bg-darkblue">
                <div class="container">
                    <div class="align-center mb-50">
                        <h2>Technology</h2>
                        <p class="text-muted">Suzuki Technology</p>
                    </div>
                    <div class="tab-technology align-center">
                        <div class="row">
                            <div class="col-2">
                                <div class="feature-icons">
                                    <a href="javascript:void(0)" data-target="#1">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_batteryless.png)"
                                            src="https://i.ibb.co/2NJ5wvR/image-712.png" alt="SUZUKI DUAL PROP SYSTEM"
                                            class="pr-1 pb-1">
                                        <span>SUZUKI DUAL PROP SYSTEM</span>
                                    </a>
                                    <a href="javascript:void(0)" data-target="#2">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_three_way_storage.png)"
                                            src="https://i.ibb.co/LJf4cb6/image-713.png" alt="DUAL INJECTOR"
                                            class="pr-1 pb-1">
                                        <span>DUAL INJECTOR</span>
                                    </a>
                                    <a href="javascript:void(0)" data-target="#3">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_three_way_storage.png)"
                                            src="https://i.ibb.co/vY8wf4x/image-714.png" alt="2-STAGE GEAR REDUCTION"
                                            class="pr-1 pb-1">
                                        <span>2-STAGE GEAR REDUCTION</span>
                                    </a>
                                    <a href="javascript:void(0)" data-target="#4">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_three_way_storage.png)"
                                            src="https://i.ibb.co/RyMD4P3/image-715.png" alt="LEAN BURN"
                                            class="pr-1 pb-1">
                                        <span>LEAN BURN</span>
                                    </a>
                                    <a href="javascript:void(0)" data-target="#5">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_three_way_storage.png)"
                                            src="https://i.ibb.co/52chB3n/image-716.png"
                                            alt="VARIABLE VALVE TIMING SYSTEM" class="pr-1 pb-1">
                                        <span>VARIABLE VALVE TIMING SYSTEM</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="feature-description">
                                    <?php 
                                        for ($i=0; $i < 10; $i++) { 
                                            # code...
                                        
                                    ?>
                                    <div id="<?php echo $i ?>" style="display:none">
                                        <img src="https://i.ibb.co/k8xgWnb/image-661-4.png" alt="">

                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="feature-icons">
                                    <a href="javascript:void(0)" data-target="#6">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_batteryless.png)"
                                            src="https://i.ibb.co/2NJ5wvR/image-712.png" alt="SUZUKI DUAL PROP SYSTEM"
                                            class="pr-1 pb-1">
                                        <span>SUZUKI DUAL PROP SYSTEM</span>
                                    </a>
                                    <a href="javascript:void(0)" data-target="#7">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_three_way_storage.png)"
                                            src="https://i.ibb.co/LJf4cb6/image-713.png" alt="DUAL INJECTOR"
                                            class="pr-1 pb-1">
                                        <span>DUAL INJECTOR</span>
                                    </a>
                                    <a href="javascript:void(0)" data-target="#8">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_three_way_storage.png)"
                                            src="https://i.ibb.co/vY8wf4x/image-714.png" alt="2-STAGE GEAR REDUCTION"
                                            class="pr-1 pb-1">
                                        <span>2-STAGE GEAR REDUCTION</span>
                                    </a>
                                    <a href="javascript:void(0)" data-target="#9">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_three_way_storage.png)"
                                            src="https://i.ibb.co/RyMD4P3/image-715.png" alt="LEAN BURN"
                                            class="pr-1 pb-1">
                                        <span>LEAN BURN</span>
                                    </a>
                                    <a href="javascript:void(0)" data-target="#10">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_three_way_storage.png)"
                                            src="https://i.ibb.co/52chB3n/image-716.png"
                                            alt="VARIABLE VALVE TIMING SYSTEM" class="pr-1 pb-1">
                                        <span>VARIABLE VALVE TIMING SYSTEM</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </section>

            <section class="product-list section-gallery section-gallery-thumb bg-white mb-50">
                <div class="containerx">
                    <div class="align-center mt-50">
                        <h2>Gallery</h2>
                    </div>
                    <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff"
                        class="swiper-container mySwiperThumb2 mt-50">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="https://i.ibb.co/XtCj0Tm/main-technology-pc-2.png" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://i.ibb.co/SXDLWhj/main-gallery-pc.png" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://i.ibb.co/K6bXbSX/32773880-l-2.png" />
                            </div>
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
                <div class="container">
                    <div thumbsSlider="" class="swiper-container mySwiperThumb">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="https://i.ibb.co/XtCj0Tm/main-technology-pc-2.png" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://i.ibb.co/SXDLWhj/main-gallery-pc.png" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://i.ibb.co/K6bXbSX/32773880-l-2.png" />
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <section class="product-prices bg-darkblue">
                <div class="container">
                    <div class="align-center mb-50">
                        <h2>Daftar Harga</h2>
                        <p>Harga On The Road Jakarta Mei 2021<br>
                            <small>*Harga tidak mengikat, dapat berubah sewaktu-waktu</small>
                        </p>
                        <div class="mt-30 mb-30">
                            <div class="group-model">DF 2.5</div>
                            <div class="price">Rp. 404.800.000</div>
                        </div>
                        <div class="group-button">
                            <a href="" class="btn btn-outline-white rounded-0">Unduh Brosur</a>
                            <a href="" class="btn btn-outline-white rounded-0">Spesifikasi</a>
                        </div>
                        <a href="" class="btn btn-outline-white rounded-0 mt-30">Ask More</a>

                    </div>
                </div>
            </section>

            <section class="product-list section-gallery section-badge">
                <div class="containerx">
                    <div class="align-center mt-50">
                        <h2>Accessories</h2>

                    </div>
                    <div class=" mt-30">
                        <div class="swiper-container mySwiper swiperNews">
                            <div class="swiper-wrapper">
                                <?php 
                                for ($i=0; $i < 9; $i++) { 
                                    # code...
                                ?>
                                <div class="swiper-slide">
                                    <div class="gallery-item">
                                        <div class="gallery-content">
                                            <span class="badge">Marine</span>
                                            <h3>Accessories of Suzuki Marine</h3>
                                        </div>
                                        <img src="https://i.ibb.co/fQjW7sL/image-63.png" class="w-100" alt="">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>

                </div>
            </section>


        </main>
        <script>
        var type = 'marine';
        </script>
        <footer id="footer">
            <div class="container">
                <div class="footer-nav">
                    <div class="nav-section">
                        <h3>Produk &amp; Layanan</h3>
                        <ul>
                            <li><a href="https://www.suzuki.co.id/automobile">Mobil Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/motorcycle">Motor Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/marine">Marine Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/pricelist">Daftar Harga Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/dealers">Temukan Dealer Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/services">Servis Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/eparts">Suku Cadang Suzuki</a></li>
                            <li><a href="https://dms.suzuki.co.id/simdms/assets/custom/productQualityUpdate.cshtml">Suzuki
                                    Quality Update</a></li>
                        </ul>
                    </div>
                    <div class="nav-section">
                        <h3>Pojok Suzuki</h3>
                        <ul>
                            <li><a href="https://www.suzuki.co.id/club/car">Car Club</a></li>
                            <li><a href="https://www.suzuki.co.id/club/motor">Motor Club</a></li>
                            <li><a href="https://www.suzuki.co.id/promo">Promo</a></li>
                            <li><a href="https://www.suzuki.co.id/news">Press Release</a></li>
                            <li><a href="https://www.suzuki.co.id/tips-trik">Tips and Trick</a></li>
                            <li><a href="https://www.suzuki.co.id/mysuzukistory">My Suzuki Story</a></li>
                            <li><a href="http://bikerscommunitysuzuki.com">Bikers Community</a></li>
                            <li><a href="https://suzukigsxseries.com">GSX Series</a></li>
                        </ul>
                    </div>
                    <div class="nav-section">
                        <h3>Lainnya</h3>
                        <ul>
                            <li><a href="https://www.suzuki.co.id/contact">Hubungi Kami</a></li>
                            <li><a href="http://www.sfi.co.id/">Suzuki Finance Indonesia</a></li>
                            <li><a href="http://www.autovalue.co.id/">Suzuki Autovalue</a></li>
                            <li><a href="https://www.suzuki.co.id/suzuki-insurance">Suzuki Insurance</a></li>
                        </ul>
                    </div>
                    <div class="nav-section">
                        <h3>Suzuki Corporate</h3>
                        <ul>
                            <li><a href="https://www.suzuki.co.id/corporate/tentang-suzuki">Tentang Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/corporate/sejarah">Sejarah</a></li>
                            <li><a href="https://www.suzuki.co.id/kunjungan-pabrik">Kunjungan Pabrik</a></li>
                            <li><a href="https://www.suzuki.co.id/corporate/penghargaan">Penghargaan</a></li>
                            <li><a href="https://www.suzuki.co.id/corporate/karir">Karir</a></li>
                            <li><a href="http://www.globalsuzuki.com/">Suzuki Global</a></li>
                            <li><a href="https://www.suzuki.co.id/corporate/csr">CSR</a></li>
                        </ul>
                    </div>
                    <div class="nav-section">
                        <h3>Hubungi Kami</h3>
                        <p class="fs-20 mb-5">Halo Suzuki<br><strong><a
                                    href="tel:0800-1100-800">0800-1100-800</a></strong></p>
                        <p class="fs-12 text-muted">Bebas Pulsa, 24 Jam Siaga Melayani Anda</p>
                        <a href="https://play.google.com/store/apps/details?id=com.sim.mysuzuki" target="_blank">
                            <span>Download My Suzuki</span>
                            <img alt="" src="https://suzukicdn.net/themes/default2019/img/mysuzuki.png" alt="">

                        </a>
                        <a href="https://www.suzuki.co.id/ecstar" target="_blank">
                            <img alt="" src="https://suzukicdn.net/themes/default2019/img/ecstar3.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="footer-nav row">
                    <div class="col-6">
                        <h3>suzuki mobil</h3>
                        <p class="fs-24">
                            <a href="https://id-id.facebook.com/suzukiindonesia" target="_blank" class="mr-5">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a href="https://twitter.com/suzukiindonesia" target="_blank" class="mr-5">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="https://www.instagram.com/suzuki_id" target="_blank" class="mr-5">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a href="https://www.youtube.com/user/SuzukiID" target="_blank" class="mr-5">
                                <i class="fa fa-youtube"></i>
                            </a>
                        </p>
                    </div>
                    <div class="col-6">
                        <h3>suzuki motor</h3>
                        <p class="fs-24">
                            <a href="https://id-id.facebook.com/SuzukiMotorcyclesID" target="_blank" class="mr-5">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a href="https://twitter.com/suzukimotorid" target="_blank" class="mr-5">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="https://www.instagram.com/suzukiindonesiamotor" target="_blank" class="mr-5">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a href="https://www.youtube.com/suzukimotorcyclesindonesia" target="_blank" class="mr-5">
                                <i class="fa fa-youtube"></i>
                            </a>
                        </p>
                    </div>
                </div>
                <div class="copyright scbottom">© 2019 Suzuki Indonesia. All rights reserved.</div>
            </div>
        </footer>
    </div>
    <script src="https://suzukicdn.net/themes/default2019/js/aos.js"></script>
    <script src="https://suzukicdn.net/themes/default2019/js/owl.carousel.min.js"></script>
    <script src="https://suzukicdn.net/themes/default2019/js/app.min.js?v=1.1.25"></script>
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>

    <script src="https://suzukicdn.net/themes/default2019/js/bootstrap.min.js"></script>
    <script src="https://suzukicdn.net/themes/default2019/js/marine.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script>
    $('.feature-description div:first-child').show();
    $('.feature-icons a:first-child').addClass('active');
    $('.feature-icons a').click(function() {
        var target = $(this).attr('data-target');
        $(target).siblings().hide();
        $(target).fadeIn();
        $(this).siblings('.active').removeClass('active');
        $(this).addClass('active');
    })

    //slider
    var swiper = new Swiper(".mySwiper", {
        slidesPerView: 3,
        spaceBetween: 10,
        slideToClickedSlide: true,
        loop: true,
        centeredSlides: true,
        pagination: {
            el: ".swiper-pagination",
            dynamicBullets: true,
        },
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 40,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 50,
            },
        },
    });
    var swiper = new Swiper(".mySwiperNews", {
        slidesPerView: 3,
        spaceBetween: 10,
        slideToClickedSlide: true,
        loop: true,
        centeredSlides: true,
        pagination: {
            el: ".swiper-pagination",
            dynamicBullets: true,
        },
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 40,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 50,
            },
        },
    });

    //thumb
    var swiper = new Swiper(".mySwiperThumb", {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 6,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
    });
    var swiper2 = new Swiper(".mySwiperThumb2", {
        loop: true,
        spaceBetween: 10,
        preloadImages: true,
        lazy: true,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        thumbs: {
            swiper: swiper,
        },
    });
    </script>
    <!-- <script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=1159525&mt_adid=185618&s1=https://www.suzuki.co.id/&s2=&s3=indonesia'></script> -->
</body>

</html>