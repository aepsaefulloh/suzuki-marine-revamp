<?php
require_once 'config.php';
require_once ROOT_PATH.'/lib/dao_utility.php';
require_once ROOT_PATH.'/lib/mysqlDao.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--[if IE]><link rel="icon" href="https://suzukicdn.net/themes/mobile2019//favicon.ico"><![endif]-->
    <title>Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia</title>
    <meta name="description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="google-site-verification" content="qStaE60pis5fkmA_6n4pOgeqXgvmFiws7fYMCkxL5Fc" />

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta property="og:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta property="og:url" content="https://www.suzuki.co.id/marine">
    <meta property="og:image" content="https://suzukicdn.net/themes/mobile2019/img/logo.png?1.1.19">
    <meta property="og:site_name" content="Suzuki Indonesia">
    <meta property="og:see_also" content="https://twitter.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.facebook.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.instagram.com/suzuki_id">

    <meta name="twitter:site" content="https://www.suzuki.co.id/marine">
    <meta name="twitter:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta name="twitter:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="twitter:image" content="https://suzukicdn.net/themes/mobile2019/img/logo.png?1.1.19">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:creator" content="@Suzukiindonesia">
    <link rel="canonical" href="https://www.suzuki.co.id/marine">

    <link href="https://suzukicdn.net/themes/mobile2019/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/mobile2019/css/aos.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/mobile2019/css/reset.min.css?v=1.1.19" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/mobile2019/css/style.min.css?v=1.1.19" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="32x32"
        href="https://suzukicdn.net/themes/mobile2019/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16"
        href="https://suzukicdn.net/themes/mobile2019/icons/favicon-16x16.png">
    <!-- <link rel="manifest" href="https://suzukicdn.net/themes/mobile2019//manifest.json"> -->
    <meta name="theme-color" content="#173e81">
    <meta name="apple-mobile-web-app-capable" content="no">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="apple-mobile-web-app-title" content="Suzuki Indonesia">
    <link rel="apple-touch-icon" href="https://suzukicdn.net/themes/mobile2019/icons/apple-touch-icon-152x152.png">
    <meta name="msapplication-TileImage"
        content="https://suzukicdn.net/themes/mobile2019/icons/msapplication-icon-144x144.png">
    <meta name="msapplication-TileColor" content="#173e81">
    <!-- <link href="<?php echo ROOT_URL?>/assets/themes/default2019/css/newMarineDefault2019.css?<?php echo rand()?>"
        rel="stylesheet"> -->
    <link href="<?php echo ROOT_URL?>/assets/themes/mobile2019/css/newMarineMobile2019.css?<?php echo rand()?>"
        rel="stylesheet">

    <script type="text/javascript">
    var base_url = 'https://www.suzuki.co.id/';
    </script>
    <script src="https://suzukicdn.net/themes/mobile2019/js/jquery.min.js"></script>



</head>

<body>

    <div id="app">
        <?php
            require_once 'include/m_header.php';
       ?>
        <main id="product" class="mt-0 product-marine newMarineMobile2019">

            <!-- Hero -->
            <section class="headline headline-mobile page-ocean pt-100 pb-50">
                <div class="container">
                    <img alt="" src="https://i.ibb.co/xDRSqw6/image-749.png" class="mt-100" style="width:50px">
                    <h2 class="mb-10">List Product</h2>
                    <p class="mb-30 mt-30">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Turpis ipsum nisl
                        iaculis et sit. Donec porttitor sagittis.</p>
                </div>
            </section>

            <section class="product-list section-feature">
                <div class="caption caption-middle">
                    <div class="container-fluid">
                        <div class="row align-center">
                            <div class="col-12">
                                <h2 class="mb-10">Suzuki Clean Ocean<br> Project
                                </h2>
                                <p>As the world’s ULTIMATE OUTBOARD MOTOR BRAND, Suzuki always remains focused on
                                    providing the ultimate marine experience, which requires a healthy and clean marine
                                    environment. Since 2011, for 9 years and counting, we have been voluntarily
                                    conducting the “Clean-up the World Campaign” every year to contribute to a better
                                    marine environment and more than 8,000 people have participated. In Japan, the
                                    campaign has been officially recognized by the Ministry of the Environment in the
                                    “Plastic Smart Campaign”.<br><br>

                                    To continue to make our utmost effort for environmental protection, it is now time
                                    for us to review how we have been contributing to the environment and society and
                                    newly determine our direction. As well as continuing our worldwide clean-up
                                    campaign, we will also commit to take responsible actions against plastic waste
                                    problems. This is how we came to make the new Slogan and Logo, “Suzuki Clean Ocean
                                    Project”, to show the world our commitment.
                                </p>
                            </div>
                        </div>
                        <div class="row align-center mt-30">
                            <div class="col-12">
                                <div class="border-box-ocean">
                                    <div class="inner-box">
                                        <div class="inner-text">
                                            <h3>Our Commitments</h3>
                                            <div class="line1"></div>
                                        </div>
                                        <div class="inner-text mt-30">
                                            <ol>
                                                <li>Clean-Up the World Campaign</li>
                                                <li>Reduce Plastic Packaging</li>
                                                <li>Collect Marine Micro-Plastic Waste</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
            </section>

            <section class="product-list section-news section bg-darkblue">
                <div class="container">
                    <div class="align-center">
                        <h2 class="text-white">Our Actions</h2>
                        <p class="text-white">Clean-Up the World Campaign, Reduce Plastic Packaging and Collect Marine
                            Micro-Plastic Waste.
                        </p>
                    </div>
                    <div class="product-list-marine cardUXFeature mt-50">
                        <div class="swiper-container mySwiper">
                            <div class="swiper-wrapper">
                                <?php 
                                for ($i=1; $i < 3; $i++) { 
                                    # code...
                                ?>
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img src="https://i.ibb.co/1rHq863/image-35.png" alt="Avatar"
                                            style="width:100%">
                                        <div class="card-body">
                                            <div class="card-text">
                                                <h5><?php echo $i ?>. Clean-Up the World Campaign</h5>
                                                <p>More than 8,000 people from 26 Suzuki distributors participated in
                                                    this activity.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="line1 mb-50"></div>
                    <div class="row">
                        <div class="col-12">
                            <img src="https://i.ibb.co/Qbqcncv/dada-1.png" class="w-100 mt-50 mb-30"
                                alt="monitoring-test">
                            <p class="text-white" style="font-size:12px;">In order to clean the ocean, the Suzuki Marine
                                Team will promote the
                                "Suzuki Clean</br>
                                Ocean
                                Project" together with partners and boat users all around the world.
                            </p>
                        </div>
                    </div>
                </div>
            </section>

        </main>
        <?php 
            require_once 'include/m_footer.php';
        ?>
    </div>

    <div class="floating-menu">
        <a href="https://www.suzuki.co.id/pricelist" target="_blank" class='flbtnpricelist'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-pricelist.png" class='flbtnpricelist'>
            Price List
        </a>
        <a href="https://www.suzuki.co.id/dealers?t=marine" target="_blank" class='flbtndealer'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-dealer.png" class='flbtndealer'>
            Dealer
        </a>
        <a href="https://www.suzuki.co.id/services/halo-suzuki" target="_blank" class='flbtnhalo'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-phone.png" class='flbtnhalo'>
            Halo Suzuki
        </a>

        <a href="https://www.suzuki.co.id/test-drive" target="_blank" class='flbtntestdrive'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-drive.png" class='flbtntestdrive'>
            Drive/Ride
        </a>


    </div>

    <script src="https://suzukicdn.net/themes/mobile2019/js/aos.js"></script>
    <script src="https://suzukicdn.net/themes/mobile2019/js/app.min.js?v=1.1.19"></script>
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>

    <script src="https://suzukicdn.net/themes/mobile2019/js/bootstrap.min.js"></script>
    <script src="https://suzukicdn.net/themes/mobile2019/js/marine.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <script>
    var swiper = new Swiper(".mySwiper", {
        slidesPerView: 1,
        spaceBetween: 10,
        slideToClickedSlide: true,
        loop: true,
        centeredSlides: true,
        pagination: {
            el: ".swiper-pagination",
            dynamicBullets: true,
        },
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 40,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 50,
            },
        },
    });
    // Tabs JS
    const btns = document.querySelectorAll(".tabs__button");
    const tabContent = document.querySelectorAll(".tab-content");

    for (let i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", () => {
            addClassFunc(btns[i], "tabs__button--active");
            clearClassFunc(i, btns, "tabs__button--active");

            addClassFunc(tabContent[i], "tab-content--active");
            clearClassFunc(i, tabContent, "tab-content--active");
        });
    }

    function addClassFunc(elem, elemClass) {
        elem.classList.add(elemClass);
    }

    function clearClassFunc(indx, elems, elemClass) {
        for (let i = 0; i < elems.length; i++) {
            if (i === indx) {
                continue;
            }
            elems[i].classList.remove(elemClass);
        }
    }
    </script>

    <!-- end webpushr tracking code -->
</body>

</html>