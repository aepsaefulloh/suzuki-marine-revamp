<?php
require_once 'config.php';
require_once ROOT_PATH.'/lib/dao_utility.php';
require_once ROOT_PATH.'/lib/mysqlDao.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--[if IE]><link rel="icon" href="https://suzukicdn.net/themes/mobile2019//favicon.ico"><![endif]-->
    <title>Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia</title>
    <meta name="description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="google-site-verification" content="qStaE60pis5fkmA_6n4pOgeqXgvmFiws7fYMCkxL5Fc" />

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta property="og:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta property="og:url" content="https://www.suzuki.co.id/marine">
    <meta property="og:image" content="https://suzukicdn.net/themes/mobile2019/img/logo.png?1.1.19">
    <meta property="og:site_name" content="Suzuki Indonesia">
    <meta property="og:see_also" content="https://twitter.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.facebook.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.instagram.com/suzuki_id">

    <meta name="twitter:site" content="https://www.suzuki.co.id/marine">
    <meta name="twitter:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta name="twitter:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="twitter:image" content="https://suzukicdn.net/themes/mobile2019/img/logo.png?1.1.19">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:creator" content="@Suzukiindonesia">
    <link rel="canonical" href="https://www.suzuki.co.id/marine">

    <link href="https://suzukicdn.net/themes/mobile2019/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/mobile2019/css/aos.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/mobile2019/css/reset.min.css?v=1.1.19" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/mobile2019/css/style.min.css?v=1.1.19" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="32x32"
        href="https://suzukicdn.net/themes/mobile2019/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16"
        href="https://suzukicdn.net/themes/mobile2019/icons/favicon-16x16.png">
    <!-- <link rel="manifest" href="https://suzukicdn.net/themes/mobile2019//manifest.json"> -->
    <meta name="theme-color" content="#173e81">
    <meta name="apple-mobile-web-app-capable" content="no">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="apple-mobile-web-app-title" content="Suzuki Indonesia">
    <link rel="apple-touch-icon" href="https://suzukicdn.net/themes/mobile2019/icons/apple-touch-icon-152x152.png">
    <meta name="msapplication-TileImage"
        content="https://suzukicdn.net/themes/mobile2019/icons/msapplication-icon-144x144.png">
    <meta name="msapplication-TileColor" content="#173e81">
    <!-- <link href="<?php echo ROOT_URL?>/assets/themes/default2019/css/newMarineDefault2019.css?<?php echo rand()?>"
        rel="stylesheet"> -->
    <link href="<?php echo ROOT_URL?>/assets/themes/mobile2019/css/newMarineMobile2019.css?<?php echo rand()?>"
        rel="stylesheet">

    <script type="text/javascript">
    var base_url = 'https://www.suzuki.co.id/';
    </script>
    <script src="https://suzukicdn.net/themes/mobile2019/js/jquery.min.js"></script>



</head>

<body>

    <div id="app">
        <?php
            require_once 'include/m_header.php';
       ?>
        <main id="product" class="mt-0 product-marine newMarineMobile2019">
            <!-- Hero -->
            <section class="headline pt-50">
                <div class="container">
                    <img alt="" src="https://i.ibb.co/xDRSqw6/image-749.png" class="mt-100" style="width:50px">
                    <h2 class="mb-10">Join The Journey</h2>
                    <p class="mb-30 mt-30">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Turpis ipsum nisl
                        iaculis et sit. Donec porttitor sagittis.</p>
                    <div style="display:flex; justify-content:space-between;">
                        <a href="https://www.suzuki.co.id/marine/DF-325-ATX" class="btn btn-white">SEE MORE</a>
                        <img alt="" src="https://i.ibb.co/Sdwttg9/image-751.png" class="" style="width:100px">
                    </div>
                </div>
            </section>
            <!-- Hero End -->
            <section class="product-list newMarine2019 pt-100">
                <div class="container">
                    <div class="product-list-marine cardGroupMarine2019">
                        <div class="card">
                            <img src="https://i.ibb.co/1rHq863/image-35.png" alt="Avatar" style="width:100%">
                            <div class="card-body">
                                <div class="card-inner">
                                    <div class="card-text">
                                        <p>Marine</p>
                                        <h5>Technology</h5>
                                    </div>
                                    <div class="card-arrow">
                                        <a href="javascript:void(0)" class="btn-modDarkBlue"><i
                                                class="fas fa-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <img src="https://i.ibb.co/Qfcdxy7/image-26.png" alt="Avatar" style="width:100%">
                            <div class="card-body">
                                <div class="card-inner">
                                    <div class="card-text">
                                        <p>Marine</p>
                                        <h5>Models</h5>
                                    </div>
                                    <div class="card-arrow">
                                        <a href="javascript:void(0)" class="btn-modDarkBlue"><i
                                                class="fas fa-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <img src="https://i.ibb.co/D5rZzX8/image-24.png" alt="Avatar" style="width:100%">
                            <div class="card-body">
                                <div class="card-inner">
                                    <div class="card-text">
                                        <p>Marine</p>
                                        <h5>Gallery</h5>
                                    </div>
                                    <div class="card-arrow">
                                        <a href="javascript:void(0)" class="btn-modDarkBlue"><i
                                                class="fas fa-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="product-list hero-about pt-100">
                <div class="caption caption-middle">
                    <div class="container">

                        <div class="imageAbout">
                            <img src="https://i.ibb.co/p4Nhdjv/image-26-1.png" class="w-50" alt="about marine"
                                class="img-path">

                        </div>

                        <div class="align-left">
                            <h2>Suzuki Clean Ocean Project</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Facilisi non pretium netus
                                tellus nulla varius. Massa potenti odio lobortis facilisi. Nec tellus platea gravida
                                scelerisque ultrices sit malesuada massa. Commodo ut enim quam ultrices enim, nulla
                                scelerisque pellentesque consectetur.</p>

                            <p class="mb-30">Quis neque placerat ut pharetra suspendisse mattis nibh.
                                Mattis magna nunc ac velit a sit.
                                Odio tellus ornare purus morbi suspendisse. Mauris elementum magna interdum viverra
                                interdum vehicula hac quis facilisis. </p>

                            <a href="javascript:void(0)" class="btn btn-white">LIHAT DETAIL</a>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Features -->
            <section class="product-list section-feature pt-100">
                <div class="caption caption-middle">
                    <div class="container">
                        <div class="row align-center">
                            <div class="col-12">
                                <h2 class="mb-10">Suzuki<br>Ultimate Technology</h2>
                                <p>Lorem ipsum is common placeholder text used to demonstrate the graphic<br> elements
                                    of a
                                    document or visual presentation.</p>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-12">
                                <ul class="feature p-0">
                                    <li>
                                        <div class="feature-inner">
                                            <h6>DURABILITY & RELIABILITY</h6>
                                            <p>Lorem ipsum dolor sit amet, consectetur.</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="feature-inner">
                                            <h6>PERFORMANCE</h6>
                                            <p>Lorem ipsum dolor sit amet, consectetur.</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="feature-inner">
                                            <h6>EASE & COMFORT</h6>
                                            <p>Lorem ipsum dolor sit amet, consectetur.</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="feature-inner">
                                            <h6>ECOLOGY & ECONOMY</h6>
                                            <p>Lorem ipsum dolor sit amet, consectetur.</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
            </section>
            <!-- Features End -->

            <section class="product-list section-news pt-100">
                <div class="container">
                    <div class="topHeading">
                        <h2>Enter the world<br>
                            of all sea</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br> Venenatis scelerisque at quam
                            congue
                            posuere libero in sit quam.<br> Consequat, scelerisque non tincidunt sit lectus senectus.
                        </p>
                    </div>
                    <img src="https://i.ibb.co/zVH3HdL/32773880-l-4.png" class="w-100 imgTopHeading" alt="">
                </div>
                <div class="container">
                    <div class="align-center mt-50">
                        <h2>News</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br> Facilisi non pretium netus
                            tellus nulla varius. Massa potenti odio lobortis facilisi. </p>
                    </div>
                    <div class="product-list-marine cardNewsMarine mt-50">
                        <div class="swiper-container mySwiper">
                            <div class="swiper-wrapper">
                                <?php 
                                for ($i=0; $i < 9; $i++) { 
                                    # code...
                                ?>
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img src="https://i.ibb.co/1rHq863/image-35.png" alt="Avatar"
                                            style="width:100%">
                                        <div class="card-body">
                                            <div class="card-text">
                                                <span class="dateNews">June 01/ 2019</span>
                                                <h5>Dolorem eum fugiat quo voluptas</h5>
                                                <p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
                                                    quam
                                                    nihil molestiae.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>

                </div>
            </section>



        </main>
        <?php 
            require_once 'include/m_footer.php';
        ?>
    </div>

    <div class="floating-menu">
        <a href="https://www.suzuki.co.id/pricelist" target="_blank" class='flbtnpricelist'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-pricelist.png" class='flbtnpricelist'>
            Price List
        </a>
        <a href="https://www.suzuki.co.id/dealers?t=marine" target="_blank" class='flbtndealer'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-dealer.png" class='flbtndealer'>
            Dealer
        </a>
        <a href="https://www.suzuki.co.id/services/halo-suzuki" target="_blank" class='flbtnhalo'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-phone.png" class='flbtnhalo'>
            Halo Suzuki
        </a>

        <a href="https://www.suzuki.co.id/test-drive" target="_blank" class='flbtntestdrive'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-drive.png" class='flbtntestdrive'>
            Drive/Ride
        </a>


    </div>

    <script src="https://suzukicdn.net/themes/mobile2019/js/aos.js"></script>
    <script src="https://suzukicdn.net/themes/mobile2019/js/app.min.js?v=1.1.19"></script>
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>

    <script src="https://suzukicdn.net/themes/mobile2019/js/bootstrap.min.js"></script>
    <script src="https://suzukicdn.net/themes/mobile2019/js/marine.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <script>
    var swiper = new Swiper(".mySwiper", {
        slidesPerView: 1,
        spaceBetween: 10,
        autoplay: {
            delay: 7500,
            disableOnInteraction: false,
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
            dynamicBullets: true,
        },
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 40,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 50,
            },
        },
    });
    </script>
    <!-- end webpushr tracking code -->
</body>

</html>