<?php
require_once 'config.php';
require_once ROOT_PATH.'/lib/dao_utility.php';
require_once ROOT_PATH.'/lib/mysqlDao.php';
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--[if IE]><link rel="icon" href="https://suzukicdn.net/themes/default2019//favicon.ico"><![endif]-->
    <title>Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia</title>
    <meta name="description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="google-site-verification" content="qStaE60pis5fkmA_6n4pOgeqXgvmFiws7fYMCkxL5Fc" />

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta property="og:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta property="og:url" content="https://www.suzuki.co.id/marine">
    <meta property="og:image" content="https://suzukicdn.net/themes/default2019/img/logo.png?1.1.25">
    <meta property="og:site_name" content="Suzuki Indonesia">
    <meta property="og:see_also" content="https://twitter.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.facebook.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.instagram.com/suzuki_id">

    <meta name="twitter:site" content="https://www.suzuki.co.id/marine">
    <meta name="twitter:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta name="twitter:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="twitter:image" content="https://suzukicdn.net/themes/default2019/img/logo.png?1.1.25">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:creator" content="@Suzukiindonesia">
    <link rel="canonical" href="https://www.suzuki.co.id/marine">

    <link href="https://suzukicdn.net/themes/default2019/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/default2019/css/aos.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/default2019/css/reset.min.css?v=1.1.25" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/default2019/css/style.min.css?v=1.1.25" rel="stylesheet">
    <link href="<?php echo ROOT_URL?>/assets/themes/default2019/css/newMarineDefault2019.css?<?php echo rand()?>"
        rel="stylesheet">

    <link rel="icon" type="image/png" sizes="32x32"
        href="https://suzukicdn.net/themes/default2019/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16"
        href="https://suzukicdn.net/themes/default2019/icons/favicon-16x16.png">
    <!-- <link rel="manifest" href="https://suzukicdn.net/themes/default2019//manifest.json"> -->
    <meta name="theme-color" content="#173e81">
    <meta name="apple-mobile-web-app-capable" content="no">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="apple-mobile-web-app-title" content="Suzuki Indonesia">
    <link rel="apple-touch-icon" href="https://suzukicdn.net/themes/default2019/icons/apple-touch-icon-152x152.png">
    <meta name="msapplication-TileImage"
        content="https://suzukicdn.net/themes/default2019/icons/msapplication-icon-144x144.png">
    <meta name="msapplication-TileColor" content="#173e81">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js"></script>
    <script type="text/javascript">
    var base_url = 'https://www.suzuki.co.id/';
    </script>
    <script src="https://suzukicdn.net/themes/default2019/js/jquery.min.js"></script>
</head>

<body>
    <div id="app">
        <?php require_once 'include/header.php'; ?>
        <main id="product" class="mt-0 product-marine newMarine2019">
            <!-- Hero -->
            <section class="hero hero-marine newMarine2019">
                <div class="bg-overlay"></div>

                <video src="<?php echo ROOT_URL?>/assets/vids.mp4?<?php echo rand()?>" autoplay loop playsinline
                    muted></video>
                <div class="caption newMarine2019">
                    <div class="container">
                        <div class="row ai-center">
                            <div class="col-7">
                                <!-- <img src="https://suzukicdn.net/themes/default2019/img/marine/logo-ultimate.png" alt=""
                        class="mb-30" style="width:400px"> -->
                                <h2 class="mb-10">Join The Journey</h2>
                                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                <p class="mb-50"> Turpis ipsum nisl
                                    iaculis et sit. Donec porttitor sagittis.</p>
                                <a href="https://www.suzuki.co.id/marine/DF-325-ATX" class="btn btn-white">LIHAT
                                    DETAIL</a>
                            </div>
                            <div class="col-5">
                                <div style="display: flex;justify-content: flex-end;">
                                    <img src="https://i.ibb.co/xDRSqw6/image-749.png" alt="" style="width:80px">
                                </div>
                                <div style="display: flex;justify-content: flex-end;">
                                    <img src="<?php echo ROOT_URL?>/assets/img/logo/ultimate.png" alt=""
                                        style="width:200px">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Hero End -->

            <!-- Navigation -->
            <section class="product-list newMarine2019">
                <div class="container">
                    <div class="product-list-marine cardGroupMarine2019">
                        <div class="row">
                            <div class="col-4">
                                <div class="card">
                                    <img src="https://i.ibb.co/1rHq863/image-35.png" alt="Avatar" style="width:100%">
                                    <div class="card-body">
                                        <div class="card-inner">
                                            <div class="card-text">
                                                <p>Marine</p>
                                                <h5>Technology</h5>
                                            </div>
                                            <div class="card-arrow">
                                                <a href="javascript:void(0)" class="btn-modDarkBlue"><i
                                                        class="fas fa-arrow-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="card">
                                    <img src="https://i.ibb.co/Qfcdxy7/image-26.png" alt="Avatar" style="width:100%">
                                    <div class="card-body">
                                        <div class="card-inner">
                                            <div class="card-text">
                                                <p>Marine</p>
                                                <h5>Models</h5>
                                            </div>
                                            <div class="card-arrow">
                                                <a href="javascript:void(0)" class="btn-modDarkBlue"><i
                                                        class="fas fa-arrow-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="card">
                                    <img src="https://i.ibb.co/D5rZzX8/image-24.png" alt="Avatar" style="width:100%">
                                    <div class="card-body">
                                        <div class="card-inner">
                                            <div class="card-text">
                                                <p>Marine</p>
                                                <h5>Gallery</h5>
                                            </div>
                                            <div class="card-arrow">
                                                <a href="javascript:void(0)" class="btn-modDarkBlue"><i
                                                        class="fas fa-arrow-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Navigation End -->

            <!-- Section About Marine -->
            <section class="hero hero-product hero-about">
                <img src="<?php echo ROOT_URL?>/assets/img/background/bg-deepsea.jpg?<?php echo rand()?>" alt=""
                    class="w-100">
                <div class="caption caption-middle">
                    <div class="container">
                        <div class="row">
                            <div class="col-4 ">
                                <div class="imageAbout">
                                    <img src="https://i.ibb.co/KL9cT6t/vector1.png" class="vector-top" alt="vector">
                                    <img src="https://i.ibb.co/p4Nhdjv/image-26-1.png" class="w-100" alt="about marine"
                                        class="img-path">
                                    <img src="https://i.ibb.co/RYKzhkg/vector2.png" class="vector-bottom" alt="vector">

                                </div>
                            </div>
                            <div class="col-7 ml-50">
                                <h2>Suzuki Clean Ocean Project</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Facilisi non pretium netus
                                    tellus nulla varius. Massa potenti odio lobortis facilisi. Nec tellus platea gravida
                                    scelerisque ultrices sit malesuada massa. Commodo ut enim quam ultrices enim, nulla
                                    scelerisque pellentesque consectetur.</p>

                                <p class="mb-30">Quis neque placerat ut pharetra suspendisse mattis nibh.
                                    Mattis magna nunc ac velit a sit.
                                    Odio tellus ornare purus morbi suspendisse. Mauris elementum magna interdum viverra
                                    interdum vehicula hac quis facilisis. </p>

                                <a href="javascript:void(0)" class="btn btn-white">LIHAT DETAIL</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Section About Marine End -->

            <!-- Features -->
            <section class="hero hero-product section-feature">
                <img src="<?php echo ROOT_URL?>/assets/img/background/ombak-biru.jpg?<?php echo rand()?>" alt=""
                    class="w-100">
                <div class="caption caption-middle">
                    <div class="container">
                        <div class="row align-center">
                            <div class="col-12">
                                <h2 class="mb-10">Suzuki<br>Ultimate Technology</h2>
                                <p>Lorem ipsum is common placeholder text used to demonstrate the graphic<br> elements
                                    of a
                                    document or visual presentation.</p>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-12">
                                <ul class="feature">
                                    <li>
                                        <div class="feature-inner">
                                            <h6>DURABILITY & RELIABILITY</h6>
                                            <p>Lorem ipsum dolor sit amet, consectetur.</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="feature-inner">
                                            <h6>PERFORMANCE</h6>
                                            <p>Lorem ipsum dolor sit amet, consectetur.</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="feature-inner">
                                            <h6>EASE & COMFORT</h6>
                                            <p>Lorem ipsum dolor sit amet, consectetur.</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="feature-inner">
                                            <h6>ECOLOGY & ECONOMY</h6>
                                            <p>Lorem ipsum dolor sit amet, consectetur.</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Features End -->

            <!-- News -->
            <section class="product-list section-news">
                <div class="container">
                    <div class="topHeading">
                        <h2>Enter the world<br>
                            of all sea</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br> Venenatis scelerisque at quam
                            congue
                            posuere libero in sit quam.<br> Consequat, scelerisque non tincidunt sit lectus senectus.
                        </p>
                    </div>
                    <img src="https://i.ibb.co/zVH3HdL/32773880-l-4.png" class="w-100 imgTopHeading" alt="">
                </div>
                <div class="container">
                    <div class="align-center mt-50">
                        <h2>News</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br> Facilisi non pretium netus
                            tellus nulla varius. Massa potenti odio lobortis facilisi. </p>
                    </div>
                    <div class="product-list-marine cardNewsMarine mt-50">
                        <div class="swiper-container mySwiper">
                            <div class="swiper-wrapper">
                                <?php 
                                for ($i=0; $i < 9; $i++) { 
                                    # code...
                                ?>
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img src="https://i.ibb.co/1rHq863/image-35.png" alt="Avatar"
                                            style="width:100%">
                                        <div class="card-body">
                                            <div class="card-text">
                                                <span class="dateNews">June 01/ 2019</span>
                                                <h5>Dolorem eum fugiat quo voluptas</h5>
                                                <p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
                                                    quam
                                                    nihil molestiae.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>

                </div>
            </section>
            <!-- News End -->


        </main>
        <script>
        var type = 'marine';
        </script>
        <footer id="footer">
            <div class="container">
                <div class="footer-nav">
                    <div class="nav-section">
                        <h3>Produk &amp; Layanan</h3>
                        <ul>
                            <li><a href="https://www.suzuki.co.id/automobile">Mobil Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/motorcycle">Motor Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/marine">Marine Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/pricelist">Daftar Harga Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/dealers">Temukan Dealer Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/services">Servis Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/eparts">Suku Cadang Suzuki</a></li>
                            <li><a href="https://dms.suzuki.co.id/simdms/assets/custom/productQualityUpdate.cshtml">Suzuki
                                    Quality Update</a></li>
                        </ul>
                    </div>
                    <div class="nav-section">
                        <h3>Pojok Suzuki</h3>
                        <ul>
                            <li><a href="https://www.suzuki.co.id/club/car">Car Club</a></li>
                            <li><a href="https://www.suzuki.co.id/club/motor">Motor Club</a></li>
                            <li><a href="https://www.suzuki.co.id/promo">Promo</a></li>
                            <li><a href="https://www.suzuki.co.id/news">Press Release</a></li>
                            <li><a href="https://www.suzuki.co.id/tips-trik">Tips and Trick</a></li>
                            <li><a href="https://www.suzuki.co.id/mysuzukistory">My Suzuki Story</a></li>
                            <li><a href="http://bikerscommunitysuzuki.com">Bikers Community</a></li>
                            <li><a href="https://suzukigsxseries.com">GSX Series</a></li>
                        </ul>
                    </div>
                    <div class="nav-section">
                        <h3>Lainnya</h3>
                        <ul>
                            <li><a href="https://www.suzuki.co.id/contact">Hubungi Kami</a></li>
                            <li><a href="http://www.sfi.co.id/">Suzuki Finance Indonesia</a></li>
                            <li><a href="http://www.autovalue.co.id/">Suzuki Autovalue</a></li>
                            <li><a href="https://www.suzuki.co.id/suzuki-insurance">Suzuki Insurance</a></li>
                        </ul>
                    </div>
                    <div class="nav-section">
                        <h3>Suzuki Corporate</h3>
                        <ul>
                            <li><a href="https://www.suzuki.co.id/corporate/tentang-suzuki">Tentang Suzuki</a></li>
                            <li><a href="https://www.suzuki.co.id/corporate/sejarah">Sejarah</a></li>
                            <li><a href="https://www.suzuki.co.id/kunjungan-pabrik">Kunjungan Pabrik</a></li>
                            <li><a href="https://www.suzuki.co.id/corporate/penghargaan">Penghargaan</a></li>
                            <li><a href="https://www.suzuki.co.id/corporate/karir">Karir</a></li>
                            <li><a href="http://www.globalsuzuki.com/">Suzuki Global</a></li>
                            <li><a href="https://www.suzuki.co.id/corporate/csr">CSR</a></li>
                        </ul>
                    </div>
                    <div class="nav-section">
                        <h3>Hubungi Kami</h3>
                        <p class="fs-20 mb-5">Halo Suzuki<br><strong><a
                                    href="tel:0800-1100-800">0800-1100-800</a></strong></p>
                        <p class="fs-12 text-muted">Bebas Pulsa, 24 Jam Siaga Melayani Anda</p>
                        <a href="https://play.google.com/store/apps/details?id=com.sim.mysuzuki" target="_blank">
                            <span>Download My Suzuki</span>
                            <img alt="" src="https://suzukicdn.net/themes/default2019/img/mysuzuki.png" alt="">

                        </a>
                        <a href="https://www.suzuki.co.id/ecstar" target="_blank">
                            <img alt="" src="https://suzukicdn.net/themes/default2019/img/ecstar3.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="footer-nav row">
                    <div class="col-6">
                        <h3>suzuki mobil</h3>
                        <p class="fs-24">
                            <a href="https://id-id.facebook.com/suzukiindonesia" target="_blank" class="mr-5">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a href="https://twitter.com/suzukiindonesia" target="_blank" class="mr-5">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="https://www.instagram.com/suzuki_id" target="_blank" class="mr-5">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a href="https://www.youtube.com/user/SuzukiID" target="_blank" class="mr-5">
                                <i class="fa fa-youtube"></i>
                            </a>
                        </p>
                    </div>
                    <div class="col-6">
                        <h3>suzuki motor</h3>
                        <p class="fs-24">
                            <a href="https://id-id.facebook.com/SuzukiMotorcyclesID" target="_blank" class="mr-5">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a href="https://twitter.com/suzukimotorid" target="_blank" class="mr-5">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="https://www.instagram.com/suzukiindonesiamotor" target="_blank" class="mr-5">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <a href="https://www.youtube.com/suzukimotorcyclesindonesia" target="_blank" class="mr-5">
                                <i class="fa fa-youtube"></i>
                            </a>
                        </p>
                    </div>
                </div>
                <div class="copyright scbottom">© 2019 Suzuki Indonesia. All rights reserved.</div>
            </div>
        </footer>
    </div>
    <script src="https://suzukicdn.net/themes/default2019/js/aos.js"></script>
    <script src="https://suzukicdn.net/themes/default2019/js/owl.carousel.min.js"></script>
    <script src="https://suzukicdn.net/themes/default2019/js/app.min.js?v=1.1.25"></script>
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>

    <script src="https://suzukicdn.net/themes/default2019/js/bootstrap.min.js"></script>
    <script src="https://suzukicdn.net/themes/default2019/js/marine.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <!-- <script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=1159525&mt_adid=185618&s1=https://www.suzuki.co.id/&s2=&s3=indonesia'></script> -->
    <script>
    var swiper = new Swiper(".mySwiper", {
        slidesPerView: 3,
        spaceBetween: 10,
        autoplay: {
            delay: 7500,
            disableOnInteraction: false,
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
            dynamicBullets: true,
        },
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 40,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 50,
            },
        },
    });
    </script>
</body>

</html>