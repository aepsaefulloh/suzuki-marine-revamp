<?php
require_once 'config.php';
require_once ROOT_PATH.'/lib/dao_utility.php';
require_once ROOT_PATH.'/lib/mysqlDao.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!--[if IE]><link rel="icon" href="https://suzukicdn.net/themes/mobile2019//favicon.ico"><![endif]-->
    <title>Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia</title>
    <meta name="description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="google-site-verification" content="qStaE60pis5fkmA_6n4pOgeqXgvmFiws7fYMCkxL5Fc" />

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta property="og:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta property="og:url" content="https://www.suzuki.co.id/marine">
    <meta property="og:image" content="https://suzukicdn.net/themes/mobile2019/img/logo.png?1.1.19">
    <meta property="og:site_name" content="Suzuki Indonesia">
    <meta property="og:see_also" content="https://twitter.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.facebook.com/suzukiindonesia">
    <meta property="og:see_also" content="https://www.instagram.com/suzuki_id">

    <meta name="twitter:site" content="https://www.suzuki.co.id/marine">
    <meta name="twitter:title" content="Mesin Kapal Suzuki - The Ultimate Outboard Motor | Suzuki Indonesia">
    <meta name="twitter:description"
        content="Berbagai pilihan mesin kapal Suzuki dengan tekonologi dan desain yang menarik untuk kebutuhan Anda.">
    <meta name="twitter:image" content="https://suzukicdn.net/themes/mobile2019/img/logo.png?1.1.19">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:creator" content="@Suzukiindonesia">
    <link rel="canonical" href="https://www.suzuki.co.id/marine">

    <link href="https://suzukicdn.net/themes/mobile2019/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/mobile2019/css/aos.css" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/mobile2019/css/reset.min.css?v=1.1.19" rel="stylesheet">
    <link href="https://suzukicdn.net/themes/mobile2019/css/style.min.css?v=1.1.19" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="32x32"
        href="https://suzukicdn.net/themes/mobile2019/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16"
        href="https://suzukicdn.net/themes/mobile2019/icons/favicon-16x16.png">
    <!-- <link rel="manifest" href="https://suzukicdn.net/themes/mobile2019//manifest.json"> -->
    <meta name="theme-color" content="#173e81">
    <meta name="apple-mobile-web-app-capable" content="no">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="apple-mobile-web-app-title" content="Suzuki Indonesia">
    <link rel="apple-touch-icon" href="https://suzukicdn.net/themes/mobile2019/icons/apple-touch-icon-152x152.png">
    <meta name="msapplication-TileImage"
        content="https://suzukicdn.net/themes/mobile2019/icons/msapplication-icon-144x144.png">
    <meta name="msapplication-TileColor" content="#173e81">
    <!-- <link href="<?php echo ROOT_URL?>/assets/themes/default2019/css/newMarineDefault2019.css?<?php echo rand()?>"
        rel="stylesheet"> -->
    <link href="<?php echo ROOT_URL?>/assets/themes/mobile2019/css/newMarineMobile2019.css?<?php echo rand()?>"
        rel="stylesheet">

    <script type="text/javascript">
    var base_url = 'https://www.suzuki.co.id/';
    </script>
    <script src="https://suzukicdn.net/themes/mobile2019/js/jquery.min.js"></script>



</head>

<body>

    <div id="app">
        <?php
            require_once 'include/m_header.php';
       ?>
        <main id="product" class="mt-0 product-marine newMarineMobile2019 blank">

            <!-- Hero -->
            <section class="headline headline-mobile page-detail-big pt-100 pb-100">
                <div class="container">
                    <h2 class="mb-10 pt-100">Suzuki DF325ATX</h2>
                    <p class="mb-30 mt-30"><strong>The Ultimate Outboard</strong></p>

                </div>
            </section>
            <section class="hero hero-product ">
                <img src="<?php echo ROOT_URL?>/assets/themes/mobile2019/img/detail/big/tech.png" alt="" class="w-100">
                <div class="caption pb-50">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12" style="text-align:start;">
                                <h4 class="mb-10">POWER WITH PASSION</h4>
                                <p>We are proud to introduce our DF325A – an outboard that perfectly balances awesome
                                    power and thrust, with outstanding fuel-efficiency and trusted reliability all
                                    within a lightweight and stylish design.<br><br>
                                    Built with the every-day use of larger boats in mind, this market-leading outboard
                                    has been engineered to run on 91 RON fuel and, as a world-first, is the first four
                                    stroke outboard over 300 horsepower to do so.</p>
                                <img alt="" src="http://52.77.68.225/themes/default2019/img/marine/logo-ultimate.png"
                                    class="" style="width:100px">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="product-list section-news">

                <div class="container">
                    <div class="container">
                        <div class="align-center">
                            <h2 style="color: rgba(51, 51, 51, 0.8);">Features Suzuki DF325ATX</h2>
                            <p style="color: rgba(51, 51, 51, 0.6);">FEATURE.01.</p>
                        </div>
                        <div class="product-list-marine cardUXFeature mt-50">
                            <div class="swiper-container mySwiper">
                                <div class="swiper-wrapper">
                                    <?php 
                                for ($i=1; $i < 3; $i++) { 
                                    # code...
                                ?>
                                    <div class="swiper-slide">
                                        <div class="card">
                                            <img src="https://i.ibb.co/1rHq863/image-35.png" alt="Avatar"
                                                style="width:100%">
                                            <div class="card-body">
                                                <div class="card-text">
                                                    <h5 style="color: #333333;">91 RON FUEL
                                                    </h5>
                                                    <p style="color: #333333;">Lorem ipsum dolor sit amet, consectetur
                                                        adipiscing elit. Vehicula consequat, orci senectus quis suscipit
                                                        ultrices aliquet vel. Quisque semper feugiat felis ultrices
                                                        aliquam orci sem ultricies diam. Tincidunt turpis enim ut amet..
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <section class="hero hero-product ">
                <img src="<?php echo ROOT_URL?>/assets/themes/mobile2019/img/detail/big/main_gallery_pc.jpg" alt=""
                    class="w-100">
            </section>

            <section class="product-prices bg-darkblue">
                <div class="container">
                    <div class="align-center mb-50">
                        <h2>Technology</h2>
                        <p class="text-muted">Suzuki Technology</p>
                    </div>
                    <div class="tab-technology align-center">
                        <div class="row">
                            <div class="col-12">
                                <div class="feature-icons">
                                    <a href="javascript:void(0)" data-target="#1">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_batteryless.png)"
                                            src="https://i.ibb.co/2NJ5wvR/image-712.png" alt="SUZUKI DUAL PROP SYSTEM"
                                            class="pr-1 pb-1">
                                        <span>SUZUKI DUAL PROP SYSTEM</span>
                                    </a>
                                    <a href="javascript:void(0)" data-target="#2">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_three_way_storage.png)"
                                            src="https://i.ibb.co/LJf4cb6/image-713.png" alt="DUAL INJECTOR"
                                            class="pr-1 pb-1">
                                        <span>DUAL INJECTOR</span>
                                    </a>
                                    <a href="javascript:void(0)" data-target="#3">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_three_way_storage.png)"
                                            src="https://i.ibb.co/vY8wf4x/image-714.png" alt="2-STAGE GEAR REDUCTION"
                                            class="pr-1 pb-1">
                                        <span>2-STAGE GEAR REDUCTION</span>
                                    </a>
                                    <a href="javascript:void(0)" data-target="#4">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_three_way_storage.png)"
                                            src="https://i.ibb.co/RyMD4P3/image-715.png" alt="LEAN BURN"
                                            class="pr-1 pb-1">
                                        <span>LEAN BURN</span>
                                    </a>
                                    <a href="javascript:void(0)" data-target="#5">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_three_way_storage.png)"
                                            src="https://i.ibb.co/52chB3n/image-716.png"
                                            alt="VARIABLE VALVE TIMING SYSTEM" class="pr-1 pb-1">
                                        <span>VARIABLE VALVE TIMING SYSTEM</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="feature-description my-30">
                                    <?php 
                                        for ($i=0; $i < 10; $i++) { 
                                            # code...
                                        
                                    ?>
                                    <div id="<?php echo $i ?>" style="display:none">
                                        <img src="<?php echo ROOT_URL?>/assets/themes/mobile2019/img/detail/big/engine.png"
                                            alt="">

                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="feature-icons">
                                    <a href="javascript:void(0)" data-target="#6">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_batteryless.png)"
                                            src="https://i.ibb.co/2NJ5wvR/image-712.png" alt="SUZUKI DUAL PROP SYSTEM"
                                            class="pr-1 pb-1">
                                        <span>SUZUKI DUAL PROP SYSTEM</span>
                                    </a>
                                    <a href="javascript:void(0)" data-target="#7">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_three_way_storage.png)"
                                            src="https://i.ibb.co/LJf4cb6/image-713.png" alt="DUAL INJECTOR"
                                            class="pr-1 pb-1">
                                        <span>DUAL INJECTOR</span>
                                    </a>
                                    <a href="javascript:void(0)" data-target="#8">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_three_way_storage.png)"
                                            src="https://i.ibb.co/vY8wf4x/image-714.png" alt="2-STAGE GEAR REDUCTION"
                                            class="pr-1 pb-1">
                                        <span>2-STAGE GEAR REDUCTION</span>
                                    </a>
                                    <a href="javascript:void(0)" data-target="#9">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_three_way_storage.png)"
                                            src="https://i.ibb.co/RyMD4P3/image-715.png" alt="LEAN BURN"
                                            class="pr-1 pb-1">
                                        <span>LEAN BURN</span>
                                    </a>
                                    <a href="javascript:void(0)" data-target="#10">
                                        <img alt="get_alt(https://www.suzuki.co.id/uploads/marine/features/icon_three_way_storage.png)"
                                            src="https://i.ibb.co/52chB3n/image-716.png"
                                            alt="VARIABLE VALVE TIMING SYSTEM" class="pr-1 pb-1">
                                        <span>VARIABLE VALVE TIMING SYSTEM</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="product-list section-gallery section-gallery-thumb bg-white mb-50">
                <div class="containerx">
                    <div class="align-center">
                        <h2>Gallery</h2>
                    </div>
                    <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff"
                        class="swiper-container mySwiperThumb2 mt-50">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="https://i.ibb.co/XtCj0Tm/main-technology-pc-2.png" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://i.ibb.co/SXDLWhj/main-gallery-pc.png" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://i.ibb.co/K6bXbSX/32773880-l-2.png" />
                            </div>
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
                <div class="container">
                    <div thumbsSlider="" class="swiper-container mySwiperThumb">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="https://i.ibb.co/XtCj0Tm/main-technology-pc-2.png" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://i.ibb.co/SXDLWhj/main-gallery-pc.png" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://i.ibb.co/K6bXbSX/32773880-l-2.png" />
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <section class="product-prices bg-darkblue">
                <div class="container">
                    <div class="align-center mt-50">
                        <h2 class="text-white">Daftar Harga</h2>
                        <p class="text-white">Harga On The Road Jakarta Mei 2021<br>
                            <small>*Harga tidak mengikat, dapat berubah sewaktu-waktu</small>
                        </p>
                        <div class="mt-30 mb-30">
                            <div class="group-model text-white">DF 325 ATX</div>
                            <div class="price text-white">Rp. 404.800.000</div>
                        </div>
                        <div class="group-button">
                            <a href="" class="btn btn-outline-black">Unduh Brosur</a>
                            <a href="" class="btn btn-outline-black">Spesifikasi</a>
                        </div>
                    </div>
                </div>
            </section>

            <section class="product-list section-gallery section-badge ">
                <div class="containerx">
                    <div class="align-center">
                        <h2>Accessories</h2>

                    </div>
                    <div class=" mt-30">
                        <div class="swiper-container mySwiper swiper-accessories">
                            <div class="swiper-wrapper">
                                <?php 
                                for ($i=0; $i < 9; $i++) { 
                                    # code...
                                ?>
                                <div class="swiper-slide">
                                    <div class="gallery-item">
                                        <div class="gallery-content">
                                            <span class="badge">Marine</span>
                                            <h3>Accessories of Suzuki Marine</h3>
                                        </div>
                                        <img src="https://i.ibb.co/fQjW7sL/image-63.png" class="w-100" alt="">
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>

                </div>
            </section>

            <?php 
            require_once 'include/m_footer.php';
        ?>
    </div>

    <div class="floating-menu">
        <a href="https://www.suzuki.co.id/pricelist" target="_blank" class='flbtnpricelist'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-pricelist.png" class='flbtnpricelist'>
            Price List
        </a>
        <a href="https://www.suzuki.co.id/dealers?t=marine" target="_blank" class='flbtndealer'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-dealer.png" class='flbtndealer'>
            Dealer
        </a>
        <a href="https://www.suzuki.co.id/services/halo-suzuki" target="_blank" class='flbtnhalo'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-phone.png" class='flbtnhalo'>
            Halo Suzuki
        </a>

        <a href="https://www.suzuki.co.id/test-drive" target="_blank" class='flbtntestdrive'>
            <img alt="" src="https://suzukicdn.net/themes/mobile2019/img/icon-drive.png" class='flbtntestdrive'>
            Drive/Ride
        </a>


    </div>

    <script src="https://suzukicdn.net/themes/mobile2019/js/aos.js"></script>
    <script src="https://suzukicdn.net/themes/mobile2019/js/app.min.js?v=1.1.19"></script>
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>

    <script src="https://suzukicdn.net/themes/mobile2019/js/bootstrap.min.js"></script>
    <script src="https://suzukicdn.net/themes/mobile2019/js/marine.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script>
    $('.feature-description div:first-child').show();
    $('.feature-icons a:first-child').addClass('active');
    $('.feature-icons a').click(function() {
        var target = $(this).attr('data-target');
        $(target).siblings().hide();
        $(target).fadeIn();
        $(this).siblings('.active').removeClass('active');
        $(this).addClass('active');
    })

    var swiper = new Swiper(".mySwiper", {
        slidesPerView: 1,
        spaceBetween: 10,
        slideToClickedSlide: true,
        loop: true,
        centeredSlides: true,
        pagination: {
            el: ".swiper-pagination",
            dynamicBullets: true,
        },
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 40,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 50,
            },
        },
    });

    //thumb
    var swiper = new Swiper(".mySwiperThumb", {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 4,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
    });
    var swiper2 = new Swiper(".mySwiperThumb2", {
        loop: true,
        spaceBetween: 10,
        preloadImages: true,
        lazy: true,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        thumbs: {
            swiper: swiper,
        },
    });

    var swiper = new Swiper(".mySwiper.swiper-accessories", {
        slidesPerView: 1,
        spaceBetween: 10,
        autoplay: {
            delay: 7500,
            disableOnInteraction: false,
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
            dynamicBullets: true,
        },
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 40,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 50,
            },
        },
    });
    </script>

    <!-- end webpushr tracking code -->
</body>

</html>